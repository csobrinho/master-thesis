# Master Thesis

## Objective
A dissertation presented to the University of Aveiro for the fulfillment of the requirement to obtain the Master degree in Electronic and Telecommunications Engineering.

## Scientific guidance of:
 * Prof. Dr. Jo�o Manuel Rodrigues, Auxiliary Professor of the Department of Electronics, Telecommunications and Informatics (DETI) of the University of Aveiro.
 * Prof. Dr. Nuno Lau, Auxiliary Professor of the Department of Electronics, Telecommunications and Informatics (DETI) of the University of Aveiro.