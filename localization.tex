\chapter{Localization}
\label{chap:localization}

Self-awareness of robot systems is underdeveloped and is still in its initials steps but has become a crucial field in the robotic field. Error-free systems are less and less frequent since the robots are moving from the static plant factories into unpredictable and volatile environment like a soccer field \cite{kalman_3}\cite{location_1}.

In the soccer context the minimum requirement is to know where one is, who is near, where the ball is and where the opponents are. Considering the following sequence: finding the ball, rotating around it until it detects a straight line to the nearest goal and kicking it, at some point this sequence will lead to a goal. 

But what about different robots? How should they interact with each other? If two or more robots (not necessarily from the same team) executing the same simple task one robot might block the other and the first assumption of the future goal might never occur. What if the robot scores in his own goal? Also, having all robots moving to a certain position will certainly create open spaces in the field unguarded. Localization contributes to solve all these problems.

One can even optimize the general tactics to keep the slowest or farthest robots as defenders and instruct the quickest or the closest to reach the ball.

It is easy to understand that there are many reasons for searching better algorithms and getting knowledge about the global position of the players. In terms of  tactics and team-play letting all the robots of the team know each other's positions and also the whereabouts of other opponents will certainly be a powerful tool in this competitive environment. By knowing their global positions on the field, the robots can position themselves on strategic locations (and avoid restricted areas), coordinate passes, or evaluate the risks of the current situation.

Self-localization is the task of finding the robot's own position and orientation relative to a certain static or dynamic point. In a robotic context, it implies the application of appropriate sensor fusion algorithms. There are a few different algorithms or variants that can be applied to self-localization: Kalman Filter (KF) with its Extended form (EKF), Particle Filter (PF), Tribots localization, Markov localization, Cox filter and Monte Carlo localization (MCL) \cite{localization_2}. This thesis will give a brief mathematical description of the first three in the next sections.

As a remark, in the robotic soccer scope the field (map) is known in advance so there is no apparent reason to use Simultaneous Localization and Mapping (SLAM) algorithms that do mapping and localization at the same time.

\section{Kalman filter}

The Kalman Filter is an optimal recursive data processing algorithm \cite{kalman_1}\cite{pr}. It is a set of mathematical equations that provide an efficient means to estimate the state of the process based on the previous or initial state of the process, a set of control commands and a set of measurements \cite{kalman_2}.

The basic idea is to predict the state of a process based on the previous state and the control commands, and afterwards correct those predictions based on the measurements taken. Figure \ref{fig:kalman_filter} shows a illustration of the process.

Let $x_k \in \mathbb{R}^n$ be the process state vector at the time step $k$. The process is governed by a linear stochastic difference equation, with a measurement $z_k \in \mathbb{R}^n$:

\begin{align}
x_k =& Ax_{k-1} + Bu_{k-1} + w_{k-1} \\
z_k =& H x_k + v_k
\end{align}
with $A$ begin the state transition matrix, $B$ the control matrix, $H$ the observation matrix, $u_k$ the control vector and $w_k$ a random Gaussian variable that models the randomness in the state transition.

The filter assumes that the noise is a Gaussian distribution with 0 median and known variance.

One attribute that makes the Kalman Filter optimal is that any number of sensor data or sensor devices can be used to correct the current prediction and drastically increase the fidelity of the prediction and can, therefore, be used to fuse the measurements of several sensors into a state estimation with minimized error \cite{kalman_3}.

\begin{figure}[h]
\centering
  \subfigure[]{
    \label{fig:kalman_a}
	\includegraphics[scale=1]{Pictures/kalman_a.pdf}}
  \qquad
  \subfigure[]{
    \label{fig:kalman_b}
	\includegraphics[scale=1]{Pictures/kalman_b.pdf}}
  \qquad
  \subfigure[]{
    \label{fig:kalman_c}
	\includegraphics[scale=1]{Pictures/kalman_c.pdf}}
  \qquad
  \subfigure[]{
    \label{fig:kalman_d}
	\includegraphics[scale=1]{Pictures/kalman_d.pdf}}
  \qquad
  \subfigure[]{
    \label{fig:kalman_e}
	\includegraphics[scale=1]{Pictures/kalman_e.pdf}}
  \qquad
  \subfigure[]{
    \label{fig:kalman_f}
	\includegraphics[scale=1]{Pictures/kalman_f.pdf}}
\caption{(a) Illustration of Kalman filters: (a) initial belief, (b) a measurement (in bold) with the associated uncertainty, (c) belief after integrating the measurement into the belief using the Kalman filter algorithm, (d) belief after motion to the right (which introduces uncertainty), (e) a new measurement with associated uncertainty, and (f) the resulting belief. Source (a)-(f): \cite{pr}.}
\label{fig:kalman_filter}
\end{figure}

\section{Extended Kalman Filter}
The Kalman filter was originally defined to calculate state estimates of discrete-time controlled processes that are governed by linear stochastic difference equations \cite{kalman_1}, however the Extended Kalman filter (EKF) extends the application of the normal form for processes that require non-linear functions for the prediction and/or the correction step, by linearising about the current mean and covariance.

Let $x_k \in \mathbb{R}^n$ be the process state vector at the time step $k$. The process in the extended version is now governed by a non-linear stochastic difference equation, with a measurement $z_k \in \mathbb{R}^n$:

\begin{align}
x_k =& f(x_{k-1}, u_{k-1}, w_{k-1}) \\
z_k =& h(x_k,v_k)
\end{align}

\moreinfo{A very detailed mathematical description of the (Extended) Kalman Filter including the density function, stochastic processes and prediction and correction steps can be consulted in the excellent book \cite{pr}.}

\section{Particle Filter}

The particle filter (also known as sequential Monte Carlo method) is an alternative non-parametric implementation of the Bayes filter. Particle filters approximate the posterior by a finite number of parameters.

Instead of representing the distribution by a parametric form, the key idea of the particle filter is to represent the posterior distribution by a set of random state samples drawn from this distribution. Such a representation is approximate, but it is non-parametric, and therefore can represent a much broader space of distributions than, for example, Gaussians \cite{pr}. A illustration of the filter is described in figure \ref{fig:particle_filter}.

\moreinfo{As in the previous definition, a very detailed mathematical description of a Particle Filter or about the Monte Carlo method can be consulted in the excellent book \cite{pr}.}

\begin{figure}[h]
\centering
  \subfigure[]{\label{fig:particle_filter_a}\includegraphics[scale=0.25]{Pictures/particle_filter_1.png}}
  \quad
  \subfigure[]{\label{fig:particle_filter_b}\includegraphics[scale=0.25]{Pictures/particle_filter_2.png}}
  \quad
  \subfigure[]{\label{fig:particle_filter_c}\includegraphics[scale=0.25]{Pictures/particle_filter_3.png}}
  \quad
  \subfigure[]{\label{fig:particle_filter_d}\includegraphics[scale=0.25]{Pictures/particle_filter_4.png}}
  \quad
  \subfigure[]{\label{fig:particle_filter_e}\includegraphics[scale=0.25]{Pictures/particle_filter_5.png}}
  \quad
  \subfigure[]{\label{fig:particle_filter_f}\includegraphics[scale=0.25]{Pictures/particle_filter_6.png}}
  \quad
  \subfigure[]{\label{fig:particle_filter_g}\includegraphics[scale=0.25]{Pictures/particle_filter_7.png}}
  \quad
  \subfigure[]{\label{fig:particle_filter_h}\includegraphics[scale=0.25]{Pictures/particle_filter_8.png}}
  \quad
  \subfigure[]{\label{fig:particle_filter_i}\includegraphics[scale=0.25]{Pictures/particle_filter_9.png}}
\caption{Illustration of a particle filter. Each red dot corresponds to a possible position of the robot in the map. During the (a) Starting position. All the particle still have the same weight.  . Source (a)-(i): \cite{pr}.}
\label{fig:kalman_filter}
\end{figure}






\section{Tribots filter}
This algorithm was created for robotic soccer and is based on guided update steps where the localization problem is treated as an error minimization task \cite{tribots_1} but can be used in more general applications \cite{localization_2}. It relies on a Look Up Table (LUT), that is built based on the fact the map is known \emph{a priori},  which, for each position, keeps the minimum distance to the closer landmark. The algorithm begins by assuming a given pose as truthful and estimates the error between the measured distances (distances of the considered pose to the landmarks) represented by the observation model and the LUT distances (known distances of each position to the defined map landmarks).

The main objective is to maximize the fitness or in other words minimize the error between the detected landmarks and the pre-calculated LUT by randomly correcting the pose until the maximum iteration counter is reached or the error is less than a specified parameter.

Figure \ref{fig:tribots_field_lut} shows the calculated LUT using the filter custom gradient algorithm.

\begin{figure}[H]
	\centering
	\includegraphics[scale=1]{Pictures/tribots_field_lut.png}
	\caption{Illustration of a LUT map used by the algorithm. Darker areas indicate positions where the distance to the nearest line is large and brighter areas indicate positions with smaller distances. The white lines represent the lines of the RoboCup field that are defined in the rules and know \emph{a priori}.}
\label{fig:tribots_field_lut}
\end{figure}

\section{Implementation}

A special version of the Tribots algorithm (originally developed by the CAMBADA project) was adapted to work in the NAO platform. To better test the algorithm several unit tests were done and the internal structure of the code was slightly changed to accomplish this. Also the cell size was tweaked in an attempt to try to give a better performance. 

The complete listing of the code can be found in the repository of the \emph{Portuguese Team} under the folder \texttt{nao/fcpagent/src/world/localizers/tribots} along with its corresponding unit tests in \texttt{nao/fcpagent/tests/world/localizers/tribots} or in the appendix \ref{ut:tribots}. The unit tests results with pass/fail and processing times are described in annex \ref{ut:unit_tests_results}.

\todo[inline]{falar mais acerca do que foi feito com referencia das unit tests e tempo que demora na primeira vez (500ms)}

\section{Results}
Both the unit tests and the actual real device tests ran without any major problem. The robot was able to self-locate properly but with some hiccups that needed special attention. 

In some situations the initial time to locate itself was longer since the direction of the prediction is random at the start. In some other situations, the position returned was the mirror of the actual position. It is a common case since the field is symmetrical but a quick search for the goal colors would allow a quick inversion of the position. The last problem has to do with the ``kidnapped robot problem" that is common in competition. The filter must be aware of this event in order to properly reset the initial position.

As a final remark, the Cox algorithm \cite{cox} could also be a viable alternative in terms of processing time, if that ever becomes an issue, since it is less computation intensive than the Tribots. Nevertheless it does have it caveats.