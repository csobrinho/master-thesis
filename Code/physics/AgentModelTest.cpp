/*
 * AgentModelTest.cpp
 *
 *  Created on: May 10, 2011
 *      Author: Carlos Sobrinho
 */

#include <gtest/gtest.h>
#include "physics/AgentModel.h"
#include "../TestDefines.h"

class AgentModelTest: public testing::Test
{
	protected:
		virtual void SetUp()
		{
			SAgentModel::clearInstance();
		}
};

TEST_F(AgentModelTest, FootFilteredData_Check_GET_with_default_threshold)
{
	AgentModel& expectation = SAgentModel::getInstance();
	expectation.setLFootFSRFL(0.00f);
	expectation.setLFootFSRFR(0.10f);
	expectation.setLFootFSRRL(0.20f);
	expectation.setLFootFSRRR(0.30f);

	expectation.setRFootFSRFL(0.10f);
	expectation.setRFootFSRFR(0.00f);
	expectation.setRFootFSRRL(0.08f);
	expectation.setRFootFSRRR(0.09f);

	FootFilteredData footFilteredData = expectation.getFootFilteredData(); //0.1f

	EXPECT_FLOAT_EQ(FOOT_DEFAULT_TRESHOLD, footFilteredData.threshold);

	EXPECT_FLOAT_EQ(0.00f, footFilteredData.raw[0][0]);
	EXPECT_FLOAT_EQ(0.10f, footFilteredData.raw[0][1]);
	EXPECT_FLOAT_EQ(0.20f, footFilteredData.raw[0][2]);
	EXPECT_FLOAT_EQ(0.30f, footFilteredData.raw[0][3]);

	EXPECT_FLOAT_EQ(0.10f, footFilteredData.raw[1][0]);
	EXPECT_FLOAT_EQ(0.00f, footFilteredData.raw[1][1]);
	EXPECT_FLOAT_EQ(0.08f, footFilteredData.raw[1][2]);
	EXPECT_FLOAT_EQ(0.09f, footFilteredData.raw[1][3]);

	EXPECT_EQ(false, footFilteredData.filtered[0][0]);
	EXPECT_EQ(true, footFilteredData.filtered[0][1]);
	EXPECT_EQ(true, footFilteredData.filtered[0][2]);
	EXPECT_EQ(true, footFilteredData.filtered[0][3]);

	EXPECT_EQ(true, footFilteredData.filtered[1][0]);
	EXPECT_EQ(false, footFilteredData.filtered[1][1]);
	EXPECT_EQ(false, footFilteredData.filtered[1][2]);
	EXPECT_EQ(false, footFilteredData.filtered[1][3]);

	EXPECT_TRUE(footFilteredData.leftContact);
	EXPECT_FALSE(footFilteredData.rightContact);

}
TEST_F(AgentModelTest, FootFilteredData_Check_GET_with_custom_threshold)
{
	AgentModel& expectation = SAgentModel::getInstance();
	expectation.setLFootFSRFL(0.00f);
	expectation.setLFootFSRFR(0.10f);
	expectation.setLFootFSRRL(0.20f);
	expectation.setLFootFSRRR(0.30f);

	expectation.setRFootFSRFL(0.10f);
	expectation.setRFootFSRFR(0.00f);
	expectation.setRFootFSRRL(0.08f);
	expectation.setRFootFSRRR(0.09f);

	FootFilteredData footFilteredData = expectation.getFootFilteredData(0.09f);

	EXPECT_FLOAT_EQ(0.09f, footFilteredData.threshold);

	EXPECT_FLOAT_EQ(0.00f, footFilteredData.raw[0][0]);
	EXPECT_FLOAT_EQ(0.10f, footFilteredData.raw[0][1]);
	EXPECT_FLOAT_EQ(0.20f, footFilteredData.raw[0][2]);
	EXPECT_FLOAT_EQ(0.30f, footFilteredData.raw[0][3]);

	EXPECT_FLOAT_EQ(0.10f, footFilteredData.raw[1][0]);
	EXPECT_FLOAT_EQ(0.00f, footFilteredData.raw[1][1]);
	EXPECT_FLOAT_EQ(0.08f, footFilteredData.raw[1][2]);
	EXPECT_FLOAT_EQ(0.09f, footFilteredData.raw[1][3]);

	EXPECT_EQ(false, footFilteredData.filtered[0][0]);
	EXPECT_EQ(true, footFilteredData.filtered[0][1]);
	EXPECT_EQ(true, footFilteredData.filtered[0][2]);
	EXPECT_EQ(true, footFilteredData.filtered[0][3]);

	EXPECT_EQ(true, footFilteredData.filtered[1][0]);
	EXPECT_EQ(false, footFilteredData.filtered[1][1]);
	EXPECT_EQ(false, footFilteredData.filtered[1][2]);
	EXPECT_EQ(true, footFilteredData.filtered[1][3]);

	EXPECT_TRUE(footFilteredData.leftContact);
	EXPECT_TRUE(footFilteredData.rightContact);
}
TEST_F(AgentModelTest, InertialUnitValues_Check_SET_and_GET)
{
	AgentModel& expectation = SAgentModel::getInstance();
	expectation.setInertialUnitValues(1, 2, 3, 4, 5, 1.5f, 0.5f);

	InertialUnitData inertialUnitData = expectation.getInertialUnitValues();

	EXPECT_EQ(1, inertialUnitData.accelerometer.x);
	EXPECT_EQ(2, inertialUnitData.accelerometer.y);
	EXPECT_EQ(3, inertialUnitData.accelerometer.z);
	EXPECT_EQ(4, inertialUnitData.gyrometer.x);
	EXPECT_EQ(5, inertialUnitData.gyrometer.y);
	EXPECT_EQ(1.5f, inertialUnitData.filtered.x);
	EXPECT_EQ(0.5f, inertialUnitData.filtered.y);
}
TEST_F(AgentModelTest, InertialUnitValues_Check_GET_of_filtered_values)
{
	AgentModel& expectation = SAgentModel::getInstance();
	expectation.setInertialUnitValues(1, 2, 3, 4, 5, 1.5f, 0.5f);

	Vector filtered = expectation.getInertialUnitFilteredValues();

	EXPECT_EQ(1.5f, filtered.x);
	EXPECT_EQ(0.5f, filtered.y);
}
TEST_F(AgentModelTest, InertialUnitValues_Check_SET_of_filtered_values_with_unnormalized_values)
{
	AgentModel& expectation = SAgentModel::getInstance();
	expectation.setInertialUnitValues(1, 2, 3, 4, 5, 3*M_PI, -3*M_PI);

	Vector filtered = expectation.getInertialUnitFilteredValues();

	EXPECT_FLOAT_EQ(-M_PI, filtered.x);
	EXPECT_FLOAT_EQ(M_PI, filtered.y);
}

static void checkTransformationFromTorsoToVertical(float angleX, float angleY, Vector3f vx, Vector3f vy, Vector3f vz, Vector3f expect_vx, Vector3f expect_vy, Vector3f expect_vz)
{
	AgentModel& expectation = SAgentModel::getInstance();
	expectation.setInertialUnitValues(0, 0, 0, 0, 0, angleX, angleY);

	Matrix4D torsoToVertical = expectation.getTransformationFromTorsoToVertical();

	Vector3f xtransform = torsoToVertical * vx;
	Vector3f ytransform = torsoToVertical * vy;
	Vector3f ztransform = torsoToVertical * vz;

	EXPECT_NEAR(expect_vx.x, xtransform.x, ABS_MAX_ERROR_FOR_PRECISE_CALCULATIONS);
	EXPECT_NEAR(expect_vx.y, xtransform.y, ABS_MAX_ERROR_FOR_PRECISE_CALCULATIONS);
	EXPECT_NEAR(expect_vx.z, xtransform.z, ABS_MAX_ERROR_FOR_PRECISE_CALCULATIONS);

	EXPECT_NEAR(expect_vy.x, ytransform.x, ABS_MAX_ERROR_FOR_PRECISE_CALCULATIONS);
	EXPECT_NEAR(expect_vy.y, ytransform.y, ABS_MAX_ERROR_FOR_PRECISE_CALCULATIONS);
	EXPECT_NEAR(expect_vy.z, ytransform.z, ABS_MAX_ERROR_FOR_PRECISE_CALCULATIONS);

	EXPECT_NEAR(expect_vz.x, ztransform.x, ABS_MAX_ERROR_FOR_PRECISE_CALCULATIONS);
	EXPECT_NEAR(expect_vz.y, ztransform.y, ABS_MAX_ERROR_FOR_PRECISE_CALCULATIONS);
	EXPECT_NEAR(expect_vz.z, ztransform.z, ABS_MAX_ERROR_FOR_PRECISE_CALCULATIONS);

}
TEST_F(AgentModelTest, TransformationFromTorsoToVertical_Check_GET_with_0_and_0)
{
	checkTransformationFromTorsoToVertical(0, 0,
			Vector3f(1, 0, 0), Vector3f(0, 2, 0), Vector3f(0, 0, 3),
			Vector3f(1, 0, 0), Vector3f(0, 2, 0), Vector3f(0, 0, 3)
	);
}
TEST_F(AgentModelTest, TransformationFromTorsoToVertical_Check_GET_with_minus_PI_over_2_and_0)
{
	checkTransformationFromTorsoToVertical(0, -M_PI_2,
			Vector3f(1, 0, 0), Vector3f(0, 2, 0), Vector3f(0, 0, 3),
			Vector3f(0, 0, 1), Vector3f(0, 2, 0), Vector3f(-3, 0, 0)
	);
}
TEST_F(AgentModelTest, TransformationFromTorsoToVertical_Check_GET_with_PI_over_4_and_0)
{
	float sqrt2_2 = sqrt(2)/2;

	checkTransformationFromTorsoToVertical(0, M_PI_4,
			Vector3f(1, 0, 0), 				Vector3f(0, 2, 0), Vector3f(0, 0, 3),
			Vector3f(sqrt2_2, 0, -sqrt2_2), Vector3f(0, 2, 0), Vector3f(3 * sqrt2_2, 0, 3 * sqrt2_2)
	);
}
