/*
 * WorldStateTest.cpp
 *
 *  Created on: May 10, 2011
 *      Author: Carlos Sobrinho
 */

#include <gtest/gtest.h>
#include "world/WorldState.h"
#include "physics/AgentModel.h"
#include "../TestDefines.h"

static float robot_height = 0.53f;
static float robot_height_sit = 0.24f;
static float robot_height_falling_45_degrees = 0.35f;

class WorldStateTest: public testing::Test
{
	protected:
		virtual void SetUp()
		{
			AgentModel& agentmodel = SAgentModel::resetInstance();
			SWorldState::clearInstance();
			agentmodel.initialize("../../config/nao.xml");
			agentmodel.updatePosture();
		}
};
// Para varios tan/tilt da cabeca
// Deitado de barriga para cima

static void checkEstimatedHeightFromGround(float abs_max_error, float expectedHeight, bool setJoint1, float value1, int efectorId1, bool setJoint2, float value2, int efectorId2)
{
	AgentModel& agentmodel = SAgentModel::getInstance();
	WorldState& expectation = SWorldState::getInstance();

	//ijLlj3: lhip2 -> lthigh [-25, 100]
	//ijRlj3: rhip2 -> rthigh [-25, 100]
	if (setJoint1) agentmodel.setHingeJointAngle(efectorId1, value1);
	if (setJoint2) agentmodel.setHingeJointAngle(efectorId2, value2);
	agentmodel.updatePosture();

	float headToGroundInVerticalReference = expectation.getEstimatedHeightFromGround();

	EXPECT_NEAR(expectedHeight, headToGroundInVerticalReference, abs_max_error);
}
/* Disable by now since the nao.xml was updated.
static void checkEstimatedHeightFromGround(float abs_max_error, float expectedHeight, bool setLeft, float left, bool setRight, float right)
{
	checkEstimatedHeightFromGround(abs_max_error, expectedHeight, setLeft, left, Types::ijLlj3, setRight, right, Types::ijRlj3);
}

TEST_F(WorldStateTest, EstimatedHeightFromGround_Check_GET_with_default_joint_values_should_return_direct_posture)
{
	checkEstimatedHeightFromGround(ABS_MAX_ERROR_FOR_PRECISE_CALCULATIONS, robot_height, false, 0, 0, false, 0, 0);
}
TEST_F(WorldStateTest, EstimatedHeightFromGround_Check_GET_with_left_thigh_up_should_return_direct_posture)
{
	checkEstimatedHeightFromGround(ABS_MAX_ERROR_FOR_PRECISE_CALCULATIONS, robot_height, true, 90, false, 0);
}
TEST_F(WorldStateTest, EstimatedHeightFromGround_Check_GET_with_right_thigh_up_should_return_direct_posture)
{
	checkEstimatedHeightFromGround(ABS_MAX_ERROR_FOR_PRECISE_CALCULATIONS, robot_height, false, 0, true, 90);
}
TEST_F(WorldStateTest, EstimatedHeightFromGround_Check_GET_with_both_legs_up_should_return_sit_posture)
{
	checkEstimatedHeightFromGround(ABS_MAX_ERROR_FOR_PRECISE_CALCULATIONS, robot_height_sit, true, 90, true, 90);
}
TEST_F(WorldStateTest, EstimatedHeightFromGround_Check_GET_with_right_thigh_up_and_left_ankle_with_45_should_return_cos_45_of_direct_posture)
{
	AgentModel& agentmodel = SAgentModel::getInstance();

	// Set torso inclination to 45 degrees backwards
	agentmodel.setInertialUnitValues(0, 0, 0, 0, 0, 0, -M_PI_4);

	// Set ankle to 45 degrees so that it touches the floor.
	agentmodel.setHingeJointAngle(Types::ijLlj6, 45);

	checkEstimatedHeightFromGround(ABS_MAX_ERROR_FOR_SEMI_PRECISE_CALCULATIONS, robot_height_falling_45_degrees, false, 0, true, 90);
}
*/
