/*
 * ReferentialTest.cpp
 *
 *  Created on: Jun 17, 2011
 *      Author: Carlos Sobrinho
 */

#include <gtest/gtest.h>
#include "world/localizers/tribots/Referential.h"
#include "world/localizers/tribots/Vec.h"
#include "../../../TestDefines.h"

TEST(ReferentialTest, Check_toNao_with_Tribots_Vec)
{
	Vector expectation = Referential::toNao(Tribots::Vec(10, 20));
	EXPECT_EQ(20, expectation.x);
	EXPECT_EQ(-10, expectation.y);
}
TEST(ReferentialTest, Check_toTribots_with_Tribots_Vec)
{
	Tribots::Vec expectation = Referential::toTribots(Tribots::Vec(10, 20));
	EXPECT_EQ(20, expectation.x);
	EXPECT_EQ(-10, expectation.y);
}
TEST(ReferentialTest, Check_toTribots_with_Vector)
{
	Tribots::Vec expectation = Referential::toTribots(Vector(10, 20));
	EXPECT_EQ(20, expectation.x);
	EXPECT_EQ(-10, expectation.y);
}
TEST(ReferentialTest, Check_toTribots_with_empty_std_vector_Vector)
{
	std::vector<Tribots::Vec> expectation = Referential::toTribots(std::vector<Vector>());
	EXPECT_EQ(0, expectation.size());
	EXPECT_EQ(0, expectation.capacity());
}
TEST(ReferentialTest, Check_toTribots_with_std_vector_Vector_with_three_elements)
{
	std::vector<Vector> points(3);
	points[0].x = 10;
	points[0].y = 20;
	points[1].x = 30;
	points[1].y = 40;
	points[2].x = 50;
	points[2].y = 60;

	std::vector<Tribots::Vec> expectation = Referential::toTribots(points);
	EXPECT_EQ(3, expectation.size());
	EXPECT_EQ(3, expectation.capacity());

	expectation[0].x = 20;
	expectation[0].y = -10;
	expectation[1].x = 40;
	expectation[1].y = -30;
	expectation[2].x = 60;
	expectation[2].y = -50;
}
