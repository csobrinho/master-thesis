/*
 * FieldLUTTest.cpp
 *
 *  Created on: Jun 17, 2011
 *      Author: Carlos Sobrinho
 */

#include <gtest/gtest.h>
#include "world/localizers/tribots/FieldLUT.h"
#include "world/FieldOfPlay.h"
#include "../../../TestDefines.h"

class FieldLUTTest: public testing::Test
{
	protected:
		FieldOfPlay* fieldOfPlay;

		virtual void SetUp()
		{
			fieldOfPlay = new FieldOfPlay("../config/field.xml");
		}
		virtual void TearDown()
		{
			delete fieldOfPlay;
		}
};

TEST_F(FieldLUTTest, Constructor_Check_that_configFilename_is_parsed_correctly)
{
	Tribots::FieldLUT expectation = Tribots::FieldLUT(this->fieldOfPlay, 25);
}
