/*
 * TribotsLocalizerTest.cpp
 *
 *  Created on: Jun 29, 2011
 *      Author: Carlos Sobrinho
 */

#include <gtest/gtest.h>
#include "world/localizers/tribots/FieldLUT.h"
#include "world/localizers/tribots/TribotsLocalizer.h"
#include "world/localizers/tribots/Referential.h"
#include "../../../TestDefines.h"

using namespace Tribots;

class TribotsLocalizerTest: public testing::Test
{

	protected:
		FieldOfPlay* fieldOfPlay;

		virtual void SetUp()
		{
			fieldOfPlay = new FieldOfPlay("../config/field.xml");
		}
		virtual void TearDown()
		{
			delete fieldOfPlay;
		}
};

static void checkFindInitialPosition(TribotsLocalizer& expectation, Vector relativePosition, const std::vector<Vector>& whitePoints, Vector& position, float& orientation, float& quality, float& err, float minQuality, float minErr, float positionTolerance, float minOrientation, float maxOrientation)
{
	Tribots::Vec pos;
	Tribots::Angle angle;
	Vector robotPos;

	std::vector<Tribots::Vec> linesInTribotsReferential = Referential::toTribots(whitePoints);

	err = expectation.findInitialPosition(linesInTribotsReferential, MY_HALF);

	quality = expectation.getRobotPosition(pos, angle);
	position = Referential::toNao(pos);

	//FieldLUT* fieldLut = expectation.getFieldLUT();
	//fieldLut->savePGMWithPos("out.pgm", pos, orientation, linesInTribotsReferential);

	printf("Position: (%5.2f, %5.2f) Heading: %5.2f Error: %5.2f Quality: %5.2f\n", position.x, position.y, Rad2Deg(orientation), err, quality);

	EXPECT_NEAR(relativePosition.x, position.x, positionTolerance);
	EXPECT_NEAR(relativePosition.y, position.y, positionTolerance);

	EXPECT_GE(Rad2Deg(orientation), minOrientation);
	EXPECT_LE(Rad2Deg(orientation), maxOrientation);
	EXPECT_LE(err, minErr);
	EXPECT_LE(quality, minQuality);
}

TEST_F(TribotsLocalizerTest, Check_FindInitialPosition_when_robot_is_positioned_on_the_left_before_the_midfield_line)
{
	int radius = fieldOfPlay->getCenterCircleRadius();
	int cellSize = 25;
	int radiusOffset = 10;
	float i, radiusSlice = M_PI / 25;

	Vector relativePosition = Vector(-500, -300);
	std::vector<Vector> whitePoints;

	// Mid-field circle
	for (i = -M_PI; i <= M_PI; i += radiusSlice)
	{
		whitePoints.push_back(Vector(relativePosition.x + (radius - radiusOffset) * cos(i), relativePosition.y + (radius
				- radiusOffset) * sin(i)));
		whitePoints.push_back(Vector(relativePosition.x + (radius) * cos(i), relativePosition.y + (radius) * sin(i)));
		whitePoints.push_back(Vector(relativePosition.x + (radius + radiusOffset) * cos(i), relativePosition.y + (radius
				+ radiusOffset) * sin(i)));
	}

	// Horizontal mid-field line
	for (i = -radius; i <= radius; i += cellSize)
		whitePoints.push_back(Vector(relativePosition.x, relativePosition.y + i));

	TribotsLocalizer expectation = TribotsLocalizer(this->fieldOfPlay, 25);

	Vector gotPosition;
	float gotOrientation, gotQuality, gotErr;
	checkFindInitialPosition(expectation, relativePosition, whitePoints, gotPosition, gotOrientation, gotQuality, gotErr, 0.5f, 0.5f, 10, 360-3, 0+3);
}
