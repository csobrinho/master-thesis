/*
 * FieldOfPlayTest.cpp
 *
 *  Created on: Jun 28, 2011
 *      Author: Carlos Sobrinho
 */

#include <gtest/gtest.h>
#include "world/FieldOfPlay.h"
#include "../TestDefines.h"

TEST(FieldOfPlayTest, Constructor_Check_that_the_official_field_is_parsed_correctly)
{
	FieldOfPlay expectation = FieldOfPlay("../config/field.xml");

	EXPECT_TRUE(expectation.getOk());

	EXPECT_STREQ("Official", expectation.getName());
	EXPECT_STREQ("1.0", expectation.getVersion());
	EXPECT_EQ(2011, expectation.getYear());

	EXPECT_EQ(600, expectation.getCenterCircleRadius());
	EXPECT_EQ(0, expectation.getCornerArcRadius());
	EXPECT_EQ(6000, expectation.getFieldLength());
	EXPECT_EQ(4000, expectation.getFieldWidth());
	EXPECT_EQ(0, expectation.getGoalAreaLength());
	EXPECT_EQ(0, expectation.getGoalAreaWidth());
	EXPECT_EQ(450, expectation.getGoalBandWidth());
	EXPECT_EQ(800, expectation.getGoalHeight());
	EXPECT_EQ(450, expectation.getGoalLength());
	EXPECT_EQ(1400, expectation.getGoalWidth());
	EXPECT_EQ(50, expectation.getBorderLineThickness());
	EXPECT_EQ(50, expectation.getLineThickness());
	EXPECT_EQ(600, expectation.getPenaltyAreaLength());
	EXPECT_EQ(2200, expectation.getPenaltyAreaWidth());
	EXPECT_EQ(1800, expectation.getPenaltyMarkerDistance());
	EXPECT_EQ(700, expectation.getSideBandWidth());
}
TEST(FieldOfPlayTest, Constructor_Check_that_the_invalid_filename_is_detected)
{
	FieldOfPlay expectation = FieldOfPlay("../abc foo bar.xml");

	EXPECT_FALSE(expectation.getOk());

	EXPECT_STREQ("Unknown", expectation.getName());
	EXPECT_STREQ("0.0", expectation.getVersion());
	EXPECT_EQ(FIELD_NOT_DEFINED, expectation.getYear());
}
