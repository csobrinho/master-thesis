/*
 * DistanceMapTest.cpp
 *
 *  Created on: May 10, 2011
 *      Author: Carlos Sobrinho
 */

#include <gtest/gtest.h>
#include "physics/AgentModel.h"
#include "world/WorldState.h"
#include "world/DistanceMapConverter.h"
#include "world/drivers/camera/CameraDriver.h"
#include "world/drivers/camera/Ov7670.h"
#include "../TestDefines.h"

class MockOv7670: public OV7670
{
	public:
		MockOv7670(int width, int height)
		{
			this->activeResolution = Vector(width, height);
		}
		virtual const char* getManufacter() const
		{
			return "JohnDoe";
		}
		virtual const char* getName() const
		{
			return "Dummy";
		}
};
class MockWorldState: public WorldState
{
	public:
		MockWorldState(float height) :
			WorldState(), height(height)
		{
		}
		float getEstimatedHeightFromGround()
		{
			return height;
		}
	private:
		float height;
};
class DistanceMapTest: public testing::Test
{
	protected:
		virtual void SetUp()
		{
			SWorldState::setInstance(new MockWorldState(0.50f));
			SAgentModel::resetInstance();
			SCameraDriver::setInstance(new OV7670());
		}
};

TEST_F(DistanceMapTest, Constructor_Check_that_CameraDriver_is_from_singleton)
{
	DistanceMapConverter converter = DistanceMapConverter();
	CameraDriver& expectation = converter.getCameraDriver();

	EXPECT_STREQ("OmniVision", expectation.getManufacter());
	EXPECT_STREQ("OV7670", expectation.getName());
}
TEST_F(DistanceMapTest, Constructor_Check_that_CameraDriver_is_from_parameter)
{
	MockOv7670 camera(6, 6);
	DistanceMapConverter converter = DistanceMapConverter(SWorldState::getInstance(), SAgentModel::getInstance(), camera);
	CameraDriver& expectation = converter.getCameraDriver();

	EXPECT_STREQ("JohnDoe", expectation.getManufacter());
	EXPECT_STREQ("Dummy", expectation.getName());
}

static void checkPixelRelativeToMiddleOfCCD(int width, int height, int pixelx, int pixely, int expectx, int expecty)
{
	MockOv7670 camera(width, height);
	DistanceMapConverter converter = DistanceMapConverter(SWorldState::getInstance(), SAgentModel::getInstance(), camera);

	Vector expectation = converter.getPixelRelativeToMiddleOfCCD(Vector(pixelx, pixely));
	EXPECT_EQ((int)expectation.x, expectx);
	EXPECT_EQ((int)expectation.y, expecty);
}

TEST_F(DistanceMapTest, PixelRelativeToMiddleOfCCD_Check_get_when_pixel_is_in_the_middle_of_the_CCD_with_odd_resolution)
{
	checkPixelRelativeToMiddleOfCCD(9, 7, 4, 3, 0, 0);
}
TEST_F(DistanceMapTest, PixelRelativeToMiddleOfCCD_Check_get_when_pixel_is_in_the_top_middle_of_the_CCD_with_even_resolution)
{
	checkPixelRelativeToMiddleOfCCD(12, 6, 5, 2, 1, -1);
}
TEST_F(DistanceMapTest, PixelRelativeToMiddleOfCCD_Check_get_when_pixel_is_in_the_bottom_middle_of_the_CCD_with_even_resolution)
{
	checkPixelRelativeToMiddleOfCCD(12, 6, 6, 3, -1, 1);
}
TEST_F(DistanceMapTest, PixelRelativeToMiddleOfCCD_Check_get_when_pixel_is_in_the_top_left_of_the_CCD_with_odd_resolution)
{
	checkPixelRelativeToMiddleOfCCD(9, 7, 0, 0, 4, -3);
}
TEST_F(DistanceMapTest, PixelRelativeToMiddleOfCCD_Check_get_when_pixel_is_in_the_top_left_of_the_CCD_with_even_resolution)
{
	checkPixelRelativeToMiddleOfCCD(12, 6, 0, 0, 6, -3);
}
TEST_F(DistanceMapTest, PixelRelativeToMiddleOfCCD_Check_get_when_pixel_is_in_the_bottom_right_of_the_CCD_with_odd_resolution)
{
	checkPixelRelativeToMiddleOfCCD(9, 7, 9 - 1, 7 - 1, -4, 3);
}
TEST_F(DistanceMapTest, PixelRelativeToMiddleOfCCD_Check_get_when_pixel_is_in_the_bottom_right_of_the_CCD_with_even_resolution)
{
	checkPixelRelativeToMiddleOfCCD(12, 6, 12 - 1, 6 - 1, -6, 3);
}

static void checkDistance(enum CameraType type, int pixelx, int pixely, float expectx, float expecty, float errorx = ABS_MAX_ERROR_FOR_SEMI_PRECISE_CALCULATIONS, float errory = ABS_MAX_ERROR_FOR_SEMI_PRECISE_CALCULATIONS)
{
	DistanceMapConverter converter = DistanceMapConverter();
	converter.setCameraType(type);

	Vector expectation = converter.getDistance(pixelx, pixely);
	printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tExpected: (%-5.3f, %-5.3f) got (%-5.3f, %-5.3f) difference (%-5.3f, %-5.3f)\n", expectx, expecty, expectation.x, expectation.y, expectation.x - expectx, expectation.y - expecty);
	EXPECT_NEAR(expectx, expectation.x, errorx);
	EXPECT_NEAR(expecty, expectation.y, errory);
}
TEST_F(DistanceMapTest, Distance_Check_for_319x479_should_be_157x000)
{
	checkDistance(UPPER_CAMERA, 319, 479, 1.57f, 0.00f, 0.15f, 0.1f);			// 							difference (0.057, 0.002)
}
TEST_F(DistanceMapTest, Distance_Check_for_30x374_should_be_260x100)
{
	checkDistance(UPPER_CAMERA, 29, 373, 2.60f, 1.00f, 0.35f, 0.15f);			// Loss of precision in X 	difference (0.315, 0.039)
}
TEST_F(DistanceMapTest, Distance_Check_for_617x392_should_be_260x_minus100)
{
	checkDistance(UPPER_CAMERA, 617, 391, 2.60f, -1.00f, 0.15f, 0.15f);			// 							difference (-0.030, 0.064)
}
TEST_F(DistanceMapTest, Distance_Check_for_324x322_should_be_400x000)
{
	checkDistance(UPPER_CAMERA, 323, 322, 4.00f, 0.00f, 1.0f, 0.15);				// Loss of precision in X 	difference (0.706, -0.024)
}
TEST_F(DistanceMapTest, Distance_Check_for_136x320_should_be_400x100)
{
	checkDistance(UPPER_CAMERA, 135, 321, 4.00f, 1.00f, 1.0f, 0.15);				// Loss of precision in X 	difference (0.763, 0.108)
}
TEST_F(DistanceMapTest, Distance_Check_for_518x332_should_be_400x_minux100)
{
	checkDistance(UPPER_CAMERA, 517, 331, 4.00f, -1.00f, 1.0f, 0.15);			// Loss of precision in X 	difference (0.246, -0.054)
}
TEST_F(DistanceMapTest, Distance_Check_for_326x291_should_be_600x000)
{
	checkDistance(UPPER_CAMERA, 325, 290, 6.00f, 0.00f, 2.0f, 0.15);				// Loss of precision in X 	difference (1.659, -0.059)
}
TEST_F(DistanceMapTest, Distance_Check_for_348x160_should_be_045x005)
{
	MockWorldState worlstate(0.40f);
	SWorldState::setInstance(&worlstate);
	checkDistance(LOWER_CAMERA, 348, 160, 0.45f, -0.05f, 0.3f, 0.3f);
}
