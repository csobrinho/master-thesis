/*
 * OV7670Test.cpp
 *
 *  Created on: Jun 8, 2011
 *      Author: Carlos Sobrinho
 */

#include <gtest/gtest.h>
#include "world/drivers/camera/CameraDriver.h"
#include "world/drivers/camera/Ov7670.h"
#include "../../../TestDefines.h"

class OV7670Test: public testing::Test
{
	protected:
		virtual void SetUp()
		{
			SCameraDriver::clearInstance();
		}
};
TEST_F(OV7670Test, Constructor_Check_that_resolutions_are_set)
{
	OV7670 expectation = OV7670();

	EXPECT_STREQ("OmniVision", expectation.getManufacter());
	EXPECT_STREQ("OV7670", expectation.getName());
	EXPECT_EQ(3, expectation.getResolutions().size());
}
TEST_F(OV7670Test, Constructor_Check_that_active_resolution_is_640x480)
{
	OV7670 expectation = OV7670();

	EXPECT_EQ(640, expectation.getActiveResolution().x);
	EXPECT_EQ(480, expectation.getActiveResolution().y);

	expectation.setActiveResolution(2);
	EXPECT_EQ(640, expectation.getActiveResolution().x);
	EXPECT_EQ(480, expectation.getActiveResolution().y);
}
TEST_F(OV7670Test, Constructor_Check_that_active_resolution_is_160x120)
{
	OV7670 expectation = OV7670();

	expectation.setActiveResolution(0);

	EXPECT_EQ(160, expectation.getActiveResolution().x);
	EXPECT_EQ(120, expectation.getActiveResolution().y);
}
