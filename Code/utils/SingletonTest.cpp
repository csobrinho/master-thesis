/*
 * Singleton.cpp
 *
 *  Created on: May 10, 2011
 *      Author: Carlos Sobrinho
 */

#include <gtest/gtest.h>
#include "utils/Singleton.h"
#include "../TestDefines.h"

class Foo
{
	friend class Singleton<Foo>;
	friend class MockFoo;

	public:
		virtual ~Foo()
		{
			destructorCalled++;
		}
		virtual int getBar(void) const
		{
			return bar;
		}
		virtual void setBar(int bar)
		{
			this->bar = bar;
		}

		static int destructorCalled;
		static int constructorCalled;

	protected:
		Foo()
			: bar(10)
		{
			constructorCalled++;
		}
		Foo(int initial_bar)
			: bar(initial_bar)
		{
			constructorCalled++;
		}
		int bar;
};
typedef Singleton<Foo> SFoo;
int Foo::destructorCalled = 0;
int Foo::constructorCalled = 0;

class MockFoo: public Foo
{
	public:
		MockFoo()
			: Foo(40)
		{
			constructorCalled += 10;
		}
};

class SingletonTest: public testing::Test
{
	protected:
		virtual void SetUp()
		{
			SFoo::clearInstance();
		}
};

TEST_F(SingletonTest, Singleton_Check_getInstance)
{
	Foo::constructorCalled = 0;
	Foo::destructorCalled = 0;

	Foo& expectation1 = SFoo::getInstance();

	EXPECT_EQ(1, Foo::constructorCalled);
	EXPECT_EQ(0, Foo::destructorCalled);
	EXPECT_EQ(10, expectation1.getBar());

	Foo& expectation2 = SFoo::getInstance();
	Foo& expectation3 = SFoo::getInstance();
	EXPECT_EQ(10, expectation2.getBar());
	EXPECT_EQ(10, expectation3.getBar());
	EXPECT_EQ(1, Foo::constructorCalled);
	EXPECT_EQ(0, Foo::destructorCalled);
}

TEST_F(SingletonTest, Singleton_Check_resetInstance)
{
	Foo& expectation1 = SFoo::getInstance();
	expectation1.setBar(20);
	EXPECT_EQ(20, expectation1.getBar());

	Foo::constructorCalled = 0;
	Foo::destructorCalled = 0;

	Foo& expectation4 = SFoo::resetInstance();
	EXPECT_EQ(1, Foo::constructorCalled);
	EXPECT_EQ(1, Foo::destructorCalled);
	EXPECT_EQ(10, expectation4.getBar());
}
TEST_F(SingletonTest, Singleton_Check_setInstance)
{
	Foo& expectation1 = SFoo::getInstance();
	expectation1.setBar(20);
	EXPECT_EQ(20, expectation1.getBar());

	Foo::constructorCalled = 0;
	Foo::destructorCalled = 0;

	Foo* newFoo = new MockFoo();
	SFoo::setInstance(newFoo);

	EXPECT_EQ(1+10, Foo::constructorCalled);
	EXPECT_EQ(1, Foo::destructorCalled);

	Foo& expectation5 = SFoo::getInstance();
	EXPECT_EQ(1+10, Foo::constructorCalled);
	EXPECT_EQ(1, Foo::destructorCalled);
	EXPECT_EQ(40, expectation5.getBar());
}
