\section{Robot Kinematics}

\subsection{NAO Robot Specifications}
The Aldebaran NAO robot is a integrated, medium-sized humanoid robot made by Aldebaran Robotics in France. Since August 2007 that it is the official robot in the RoboCup SPL. Since the project started in 2004, it has evolved in several designs and versions. The robot is available with one to five kinematic chains (head, arms and legs) that depend on the body type. All the body types are presented in figure~\ref{fig:nao_body_types}.

\begin{figure}[htb]
\centering
  \subfigure[NAO H25]{
    \label{fig:nao_body_type_h25}
	\includegraphics[scale=0.42]{Pictures/nao_body_type_h25.png}}
  \qquad
  \subfigure[NAO H21]{
    \label{fig:nao_body_type_h21}
	\includegraphics[scale=0.42]{Pictures/nao_body_type_h21.png}}
  \subfigure[NAO T14]{
    \label{fig:nao_body_type_t14}
	\includegraphics[scale=0.42]{Pictures/nao_body_type_t14.png}}
  \qquad
  \subfigure[NAO T2]{
    \label{fig:nao_body_type_t2}
	\includegraphics[scale=0.42]{Pictures/nao_body_type_t2.png}}
\caption{Different body types of the NAO robot sold by Aldebaran Robotics}
\label{fig:nao_body_types}
\end{figure}

The body type used in the Robocup competition is the T21. This version is very similar to the T25 and mainly lacks prehensile hands and wrist joints. Within T21 and T25 there are three hardware versions (v3.2, v3.3, v4). This thesis will focus mainly on the T21 v3.3 because those were the robots used by the \emph{Portuguese Team}.

The full specifications of the v3.3 version can be checked in table \ref{tab:nao_hardware_specs_3.3} and \cite{nao_datasheet_t21}.

The robot has 21 \gls{dof}. Two on the Head, four on each arm, five on each leg and one in the pelvis. Even though the legs are independent from the pelvis down, they are coupled to the same servo (joint) and cannot be moved independently. The five kinematic chains and their respective joints are:

\begin{description}
\item[Head] HeadYaw, HeadPitch Figure \ref{fig:nao_joints_head}
\item[Left Arm] LShoulderPitch, LShoulderRoll, LElbowYaw, LElbowRoll $\to$ figure~\ref{fig:nao_joints_arms}
\item[Right Arm] RShoulderPitch, RShoulderRoll, RElbowYaw, RElbowRoll $\to$ figure~\ref{fig:nao_joints_arms}
\item[Left Leg] LHipYawPitch, LHipRoll, LHipPitch, LKneePitch, LAnklePitch, LAnkleRoll $\to$ figure~\ref{fig:nao_joints_pelvis_and_legs}
\item[Right Leg] RHipYawPitch, RHipRoll, RHipPitch, RKneePitch, RAnklePitch, RAnkleRoll $\to$ figure~\ref{fig:nao_joints_pelvis_and_legs}
\end{description}

A complete specification of the available joints with their corresponding length and operational range is described in the following sections.

\begin{table}[htb]
\caption{Detailed specifications of the NAO T21 v3.3 robot}
\label{tab:nao_hardware_specs_3.3}
\subtable{
\begin{tabular}{|c|l|}
\hline
\multicolumn{2}{|c|}{\textbf{Degrees of Freedom}} \\\hline
Head & 2 DOF \\
Arm & 4 DOF in each arm \\ 
Pelvis & 1 DOF \\
Leg & 5 DOF in each leg \\
Hand & Non prehensile \\\hline
\multicolumn{2}{|c|}{\textbf{Hardware}} \\\hline
CPU & x86 AMD Geode 500 MHz \\
Memory & 256MB SDRAM / 2GB Flash \\
\multirow{2}{*}{Network Type} & WI-FI (IEE 802.11 b/g) \\
& Ethernet\\
Height & $\sim 58 cm$ \\
Weight & $\sim 4.8 kg$ \\ 
Battery Capacity & $\sim 90 min.$ \\\hline
\end{tabular}}
\hspace{-11pt}
\subtable{
\begin{tabular}{c|l|}
\hline
\multicolumn{2}{c|}{\textbf{Sensors}} \\\hline
32x & Hall effect sensors \\
2x & Gyrometer 1 axis \\
1x & Accelerometer 3 axis \\
- & Bumpers \\
2x & Channel sonar \\
2x & Infrared \\
 & Tactile sensor \\
4x & FSR in each leg \\\hline
\multicolumn{2}{c|}{\textbf{Multimedia}} \\\hline
2x & Speakers \\
4x & Microphones \\
2x & CMOS Digital Cameras \\
 & \\\hline
\end{tabular}}
\end{table}

\begin{figure}[htb]
\centering
\mbox{
  \subfigure[Front]{
    \label{fig:nao_specs_links_front}
	\includegraphics[scale=0.57]{Pictures/nao_specs_links_front.png}}
  \smallskip
  \subfigure[Top]{
    \label{fig:nao_specs_links_top}
	\raisebox{1.5cm}{\includegraphics[scale=0.57]{Pictures/nao_specs_links_top.png}}}}
\caption{NAO T21 links and its corresponding lengths.}
\label{fig:nao_specs_links}
\end{figure}

\subsubsection{Kinematic chains and joints}
A kinematic chain refers to an assembly of rigid body segments that are connected by joints and in which each segments operate together to provide a wide range of motion for a limb (chain). The joints rotate and control the relative angular position of the links of the manipulator.

Only the combinations that don't lead to collisions between the links and/or some fixed items in the environment can be considered valid and all those form was is commonly called as the \textit{joint space}.

The term \gls{dof} defines the count of joints in a kinematic chain. More \gls{dof} lead to more freedom of the kinematic chain and more flexibility on how to achieve a point in space.

\textit{Robot kinematics} applies geometry to the study of the movement of multi-degree of freedom kinematic chains that form the structure of robotic systems \cite{robot_manipulators}. The links in the robot kinematic chain are modelled as rigid bodies and are assumed to have a pure rotation or translation. More specifically, robot kinematics provides the transformation from joint space (starting reference space) to the Cartesian space (final reference space). The opposite is also true since the transformation matrices are Affine Transformations (a detailed description is available in \ref{sec:affine_transformations}).

Their primary objective lies in the planning and execution of movements but can also be extremely useful in the calculation of forces and torques.

\subsection{Forward Kinematics}
The joint space information is very reduced in terms of the position and orientation of the end effector of the kinematic chain. The \textit{forward kinematics} defines a mapping from the joint space $\to$ three-dimensional Cartesian space. Given a \textit{kinematic chain} with $m$ joints and a set of joint values $(\theta_1,\theta_2,\ldots,\theta_m)$ the forward kinematics can find the position $(p_x,p_y,p_z)$ and the orientation $(a_x,a_y,a_z)$ of the end effector of the kinematic chain in the three-dimensional $(x,y,z)$ space.

Forward kinematics will result in what is called a closed-form, analytical solution since it will always be expressed analytically in terms of a finite number of certain ``well-known" functions.

\subsection{Inverse Kinematics}

A robot manipulator will often need to do the opposite operation of the previously described mapping that is a three-dimensional Cartesian space $\to$ \textit{joint space} conversion in order to reach a target point with a certain orientation or follow a trajectory in the three-dimensional space. This is called \textit{inverse kinematics}. In particular, the inverse kinematics defines a relation between points in the Cartesian space (let it be the position $(p_x,p_y,p_z)$ and orientation $(a_x,a_y,a_z)$) with the joint values $(\theta_1,\theta_2,\ldots,\theta_m)$ in the joint space of a kinematic chain with $m$ joints.
 
To be able to achieve this the inverse kinematics will need to find the correct values for each and every joint in the chain that will allow the manipulator to reach the intended position and orientation. Unlike the forward kinematics, the inverse kinematics yields various solutions. It is easy to imagine how many different paths (ways) a human arm could take to reach a certain point in front of it's chest. This has to do with the amount of \gls{dof} and, like the forward kinematic, more \gls{dof} will allow more solutions. This multiplicity of solutions set the inverse kinematics more as a relation than a mapping since a point in the Cartesian space may have more than one matching point in the joint space.

\section{Affine Transformations}
\label{sec:affine_transformations}
In geometry, an affine transformation or affine map or affinity is a mapping that transforms points and vectors from one space to another in such a way that the ratios of distances are kept. It does not necessarily preserve angles or lengths, but does have the property that sets of parallel lines will remain parallel to each other after an affine transformation. The source and target spaces can be $n$-dimensional with $n \geq 2$.

A point in the three-dimensional space can be represented as a column vector:
\begin{equation}
p = \begin{pmatrix}p_x \\ p_y \\ p_z\end{pmatrix} = (p_x, p_y, p_z)^\intercal
\end{equation}

The following are common examples of affine transformations: translation, geometric contraction, expansion, reflection, rotation. It is also equivalent to a linear transformation followed by a translation. Any possible combination of the above will also produce an affine transformation. The flexibility of affine transformations makes them very useful in computer graphics.

This thesis will only describe the rotation and translation affine translations since they were heavily used in the \textit{forward kinematics} and referential transformations described in section \ref{sec:joints_description}.

\subsection{Affine Transformation Matrix}
An affine transformation $f : \mathcal{A} \to \mathcal{B}$ between two affine spaces is a map that acts linearly on the vectors. The affine transformation is a composition of two functions: a translation and a linear map. Let the linear map be a multiplication of $\mathbf{A}$ and the translation of the vector $\vec{b}$, the affine map $f$ acting on vector $\vec{x}$ can be expressed as:

\begin{equation}
\label{eq:affine_matrix}
\vec{y} = f(\vec{x}) = \mathbf{A}\vec{x} + \vec{b}
\end{equation}

In order to represent both the rotation and translation in a single matrix multiplication the matrix can be rewritten using the augmented matrix (homogeneous coordinates).

\begin{equation}
\mathbf{T} = \label{eq:augmented_matrix}
\begin{bmatrix}\vec{y} \\ 1 \end{bmatrix} = \begin{bmatrix}\mathbf{A} & \vec{b}\; \\ [ 0 \cdots 0 ] & 1\end{bmatrix}
\begin{bmatrix}\vec{x} \\ 1\end{bmatrix} \quad \text{and is equivalent to \ref{eq:affine_matrix}}
\end{equation}
where $\mathbf{A}$ is a $(n \times n)$ matrix, $\vec{b}$ is a $(n \times 1)$ vector and the last line of $\mathbf{T}$ contains $n-1$ zeros followed by a $1$. The augmented matrix (\ref{eq:augmented_matrix}) is called \textit{affine transformation matrix}, or \textit{projective transformation matrix}.

As an example, applying the transformation to a given point $p=(p_1,p_2,\ldots,p_n)$ in a $n$-dimensional space would result in the multiplication of the affine transformation matrix with a column vector $v=(p_1,p_2,\ldots,p_n,1)^\intercal$:

\begin{equation}
\renewcommand{\dummy}{\vphantom{\vec{b}_x \mathbf{A_{xx}} p'_x}}
\label{eq:augmented_matrix}
v' = 
\begin{bmatrix}
	\dummy p'_1 \\
	\dummy \vdots \\
	\dummy p'_n \\
	\dummy 1 
\end{bmatrix} = 
\mathbf{T}v = 
\begin{bmatrix}
	\dummy \mathbf{A} & \dummy\vec{b}\, \\
	\dummy [ 0 \cdots 0 ] & \dummy 1\,
\end{bmatrix}
\begin{bmatrix}
	\dummy p_1 \\
	\dummy \vdots \\
	\dummy p_n \\
	\dummy 1
\end{bmatrix}
\end{equation}

For a point $p=(p_x,p_y,p_z)$ in the three-dimensional space the result will be:

\begin{equation}
\renewcommand{\dummy}{\vphantom{\vec{b}_x \mathbf{A_{xx}} p'_x}}
v' = 
\begin{bmatrix}
	\dummy p'_x \\
	\dummy p'_y \\
	\dummy p'_z \\
	\dummy 1
\end{bmatrix} = 
\mathbf{T}v = 
\begin{bmatrix}
	\dummy A_{xx} & \dummy A_{xy} & \dummy A_{xz} & \dummy b_x \\ 
	\dummy A_{yx} & \dummy A_{yy} & \dummy A_{yz} & \dummy b_y \\ 
	\dummy A_{zx} & \dummy A_{zy} & \dummy A_{zz} & \dummy b_z \\
	\dummy 0 & \dummy 0 & \dummy 0 & \dummy 1
\end{bmatrix}
\begin{bmatrix}
	\dummy p_x \\
	\dummy p_y \\
	\dummy p_z \\
	\dummy 1 
\end{bmatrix}
\end{equation}

As discussed earlier, the multiplication of two affine transformation matrices $\mathbf{T_1}$ and $\mathbf{T_2}$ is also a affine transformation:

\begin{equation}
\mathbf{T} = \mathbf{T_1}\mathbf{T_2} = 
\begin{bmatrix}\mathbf{A_1} & \vec{b_1} \\ [ 0 \cdots 0 ] & 1\end{bmatrix}
\begin{bmatrix}\mathbf{A_2} & \vec{b_2} \\ [ 0 \cdots 0 ] & 1\end{bmatrix} = 
\begin{bmatrix}\mathbf{A_1} \mathbf{A_2} & \mathbf{A_1} \vec{b_2} + \mathbf{A_2} \vec{b_2} \\ [ 0 \cdots 0 ] & 1\end{bmatrix}
\end{equation}

This property can be generalized for the product of any number of affine transformation matrices:

\begin{equation}
\mathbf{\hat{T}} = \mathbf{T_1}\mathbf{T_2} \ldots \mathbf{T_k} = 
\begin{bmatrix}\mathbf{\hat{A}} & \hat{b}\; \\ [ 0 \cdots 0 ] & 1\end{bmatrix}
\end{equation}

An affine transformation is invertible, if and only if $\mathbf{\hat{A}}$ is invertible and takes the form:

\begin{align}
\mathbf{T}^{-1} &=
\begin{bmatrix}\mathbf{A}^{-1} & -\mathbf{A}^{-1}\vec{b}\; \\ [ 0 \cdots 0 ] & 1\end{bmatrix}\qquad \text{and if } \mathbf{A} \text{ is unitary} \\
 &=
\begin{bmatrix}\mathbf{A}^{\intercal} & -\mathbf{A}^{\intercal}\vec{b}\; \\ [ 0 \cdots 0 ] & 1\end{bmatrix}
\end{align}

\subsection{Translation Matrix}
In a Cartesian space, translation is a function that moves every point preserving the distances and directions between them by a fixed distance in a specified direction. Let $d = (d_x,d_y,d_z)$ be the offset to translate and $d_x$, $d_y$ and $d_z$ the distance of translation along the $x$, $y$ and $z$ axis respectively the translation matrix $\mathbf{Z}$ would be:

\begin{equation}
\mathbf{Z} =
\begin{bmatrix}
	1 & 0 & 0 & d_x \\	
	0 & 1 & 0 & d_y \\	
	0 & 0 & 1 & d_z \\	
	0 & 0 & 0 & 1 \\	
\end{bmatrix}
\end{equation}

The translation matrix is an affine transformation matrix with $\mathbf{A} = \mathbf{I}$. To move a point $p = (p_x,p_y,p_z)^\intercal$ in the three-dimensional space by distance $(d_x,d_y,d_z)^\intercal$ the transformation to apply will be:

\begin{equation}
\renewcommand{\dummy}{\vphantom{\vec{b}_x \mathbf{A_{xx}} p'_x}}
v' = 
\begin{bmatrix}
	\dummy p'_x \\
	\dummy p'_y \\
	\dummy p'_z \\
	\dummy 1
\end{bmatrix} = 
\mathbf{Z}v = 
\begin{bmatrix}
	\dummy 1 & \dummy 0 & \dummy 0 & \dummy d_x \\ 
	\dummy 0 & \dummy 1 & \dummy 0 & \dummy d_y \\ 
	\dummy 0 & \dummy 0 & \dummy 1 & \dummy d_z \\
	\dummy 0 & \dummy 0 & \dummy 0 & \dummy 1
\end{bmatrix}
\begin{bmatrix}
	\dummy p_x \\
	\dummy p_y \\
	\dummy p_z \\
	\dummy 1 
\end{bmatrix}
\end{equation}

\subsection{Rotation Matrix}

In a Cartesian space, rotation is a function that rotates vectors by a fixed angle  in a specified direction preserving the distances and directions between. A rotation in the $n$-dimensional space is described as an $(n \times n)$ orthogonal matrix $\mathbf{R}$ with determinant $1$. This allows the following properties:

\begin{equation}
\label{eq:matrix_properties}
\mathbf{R}^\intercal = \mathbf{R}^{-1} \qquad 
\mathbf{R}\mathbf{R}^\intercal = \mathbf{R}^\intercal\mathbf{R} = \mathbf{I} \qquad
det(\mathbf{R}) = 1
\end{equation}

Three rotation matrices can be defined $\theta_x$, $\theta_y$ and $\theta_z$, one for each rotation along the $x$, $y$ and $z$ axis respectively:

\begin{equation}
\mathbf{R}_{\theta_x} = 
\begin{bmatrix}
	1 & 0 & 0 & 0 \\ 
	0 & \cos\theta_x & -\sin\theta_x & 0 \\ 
	0 & \sin\theta_x & \cos\theta_x & 0 \\ 
	0 & 0 & 0 & 1
\end{bmatrix}\qquad
\mathbf{R}_{\theta_y} = 
\begin{bmatrix}
	\cos \theta_y & 0 & \sin \theta_y & 0 \\ 
	0 & 1 & 0 & 0 \\ 
	-\sin \theta_y & 0 & \cos \theta_y & 0 \\ 
	0 & 0 & 0 & 1
\end{bmatrix}
\end{equation}
\begin{equation*}
\mathbf{R}_{\theta_z} = 
\begin{bmatrix}
	\cos\theta_z & -\sin\theta_z & 0 & 0 \\ 
	\sin\theta_z & \cos\theta_z & 0 & 0\\ 
	0 & 0 & 1 & 0 \\ 
	0 & 0 & 0 & 1
\end{bmatrix}
\end{equation*}

To rotate a vector $\vec{v}=(v_x, v_y, v_z)$ with a specific orientation $\theta = (\theta_x, \theta_y,\theta_z$) one solution would be to decompose the orientation using Euler angles in it's $\theta_x$, $\theta_y$ and $\theta_z$. Each angle would then produce a rotation transformation matrix and the multiplication in the inverted order of all the rotation transformation matrix would create the final rotation transformation matrix.

\moreinfo{Mathematical profs of the concepts introduced in this chapter are outside of the scope of this thesis but can be verified in any Analytical Geometry book and/or in \cite{analitic_geometry}.}

\subsubsection{Rotation convention using Euler angles}
Any arbitrary orientation of a rotated coordinate system can be rotated to any position using Euler rotations and can be translated so the origin of the rotated coordinate system sits on an arbitrary point in the main coordinate system. Three rotations can be performed to give any arbitrary orientation of a rotated coordinate system. By convention, the following rotations are chosen:
\begin{itemize}
	\item Rotate by angle $\gamma$ about the world $z$ axis,
	\item Rotate by angle $\beta$ about the (unrotated) world $y$ axis, and
	\item Rotate by angle $\alpha$ about the (unrotated) world $z$ axis.
\end{itemize}

These rotations are shown in figure \ref{fig:euler_angles}.

\begin{figure}[h]
	\begin{center}
\tdplotsetmaincoords{50}{140}
%
\begin{tikzpicture}[scale=2.8,tdplot_main_coords]
	\draw[thick,->] (0,0,0) -- (1,0,0) node[anchor=north east]{$x$};
	\draw[thick,->] (0,0,0) -- (0,1,0) node[anchor=north west]{$y$};
	\draw[thick,->] (0,0,0) -- (0,0,1) node[anchor=south]{$z$};

	\tdplotsetrotatedcoords{0}{0}{30}

	\draw[thick,color=red,tdplot_rotated_coords,->] (0,0,0) -- (.7,0,0) node[anchor=north]{$x'$};
	\draw[thick,color=green!50!black,tdplot_rotated_coords,->] (0,0,0) -- (0,.7,0) node[anchor=west]{$y'$};
	\draw[thick,color=blue,tdplot_rotated_coords,->] (0,0,0) -- (0,0,.7) node[anchor=west]{$z'$};
	
	\tdplotdrawarc[color=orange!50!black]{(0,0,0)}{.3}{0}{30}{anchor=north east}{$\gamma$}
\end{tikzpicture}
%
\begin{tikzpicture}[scale=2.8,tdplot_main_coords]
	\draw[thick,->] (0,0,0) -- (1,0,0) node[anchor=north east]{$x$};
	\draw[thick,->] (0,0,0) -- (0,1,0) node[anchor=north west]{$y$};
	\draw[thick,->] (0,0,0) -- (0,0,1) node[anchor=south]{$z$};

	\tdplotsetrotatedcoords{0}{0}{30}

	\draw[dashed,color=red,tdplot_rotated_coords] (0,0,0) -- (.5,0,0);
	\draw[dashed,color=green!50!black,tdplot_rotated_coords] (0,0,0) -- (0,.5,0);
	\draw[dashed,color=blue,tdplot_rotated_coords] (0,0,0) -- (0,0,.5);

	\tdplotsetrotatedcoords{0}{40}{30}

	\draw[thick,color=red,tdplot_rotated_coords,->] (0,0,0) -- (.7,0,0) node[anchor=north]{$x'$};
	\draw[thick,color=green!50!black,tdplot_rotated_coords,->] (0,0,0) -- (0,.7,0) node[anchor=west]{$y'$};
	\draw[thick,color=blue,tdplot_rotated_coords,->] (0,0,0) -- (0,0,.7) node[anchor=south]{$z'$};
	
	\tdplotsetthetaplanecoords{0}
	\tdplotdrawarc[tdplot_rotated_coords,color=orange!50!black]{(0,0,0)}{.3}{0}{40}{anchor=south}{$\beta$}

\end{tikzpicture}
%
\begin{tikzpicture}[scale=2.8,tdplot_main_coords]
	\draw[thick,->] (0,0,0) -- (1,0,0) node[anchor=north east]{$x$};
	\draw[thick,->] (0,0,0) -- (0,1,0) node[anchor=north west]{$y$};
	\draw[thick,->] (0,0,0) -- (0,0,1) node[anchor=south]{$z$};

	\tdplotsetrotatedcoords{0}{40}{30}

	\draw[dashed,color=red,tdplot_rotated_coords] (0,0,0) -- (.5,0,0);
	\draw[dashed,color=green!50!black,tdplot_rotated_coords] (0,0,0) -- (0,.5,0);
	\draw[dashed,color=blue,tdplot_rotated_coords] (0,0,0) -- (0,0,.5);

	\tdplotsetrotatedcoords{60}{0}{0}
	\draw[dotted,color=blue,tdplot_rotated_coords] (0,0,0) -- (.4,0,0);
	\tdplotsetrotatedcoords{60}{40}{30}

	\draw[thick,color=red,tdplot_rotated_coords,->] (0,0,0) -- (.7,0,0) node[anchor=north]{$x'$};
	\draw[thick,color=green!50!black,tdplot_rotated_coords,->] (0,0,0) -- (0,.7,0) node[anchor=west]{$y'$};
	\draw[thick,color=blue,tdplot_rotated_coords,->] (0,0,0) -- (0,0,.7) node[anchor=south]{$z'$};

	\tdplotdrawarc[color=orange!50!black]{(0,0,0)}{.3}{0}{60}{anchor=north east}{$\alpha$}
\end{tikzpicture}
	\end{center}
	\caption{Positioning the rotated coordinate frame $(x', y', z')$ using Euler angles $(\alpha,\beta,\gamma)$.}\label{fig:euler_angles}
\end{figure}

This rotation matrix $\mathbf{R}(\alpha,\beta,\gamma)$ is given by:
\begin{align}\begin{split}
	\mathbf{R}(\alpha,\beta,\gamma) &= \mathbf{R}_{\theta_z}(\alpha) \mathbf{R}_{\theta_y}(\beta) \mathbf{R}_{\theta_z}(\gamma)\\
	&= \begin{bmatrix}
			\cos\alpha & -\sin\alpha & 0 & 0\\
			\sin\alpha & \cos\alpha & 0 & 0\\
			0 & 0 & 1 & 0\\
			0 & 0 & 0 & 1
		\end{bmatrix}
		\begin{bmatrix}
			\cos\beta & 0 & \sin\beta & 0\\
			0 & 1 & 0 & 0\\
			-\sin\beta & 0 & \cos\beta& 0\\
			0 & 0 & 0 & 1
		\end{bmatrix}
		\begin{bmatrix}
			\cos\gamma & -\sin\gamma & 0 & 0\\
			\sin\gamma & \cos\gamma & 0 & 0\\
			0 & 0 & 1 & 0\\
			0 & 0 & 0 & 1
		\end{bmatrix}\\
	&= \begin{bmatrix}
		\cos\alpha\cos\beta\cos\gamma - \sin\alpha\sin\gamma & -\cos\alpha\cos\beta\sin\gamma - \sin\alpha\cos\gamma & \cos\alpha\sin\beta & 0\\
		\sin\alpha\cos\beta\cos\gamma + \cos\alpha\sin\gamma & -\sin\alpha\cos\beta\sin\gamma + \cos\alpha\cos\gamma & \sin\alpha\sin\beta & 0\\
		-\sin\beta\cos\gamma & \sin\beta\sin\gamma & \cos\beta & 0 \\
		0 & 0 & 0 & 1
		\end{bmatrix}
\end{split}\end{align}

\subsection{Affine Transformation Matrices and Kinematics}

In the kinematics context all rotation and translation matrices from this point forward will be defined as a single affine transformation matrix that combine rotation and translation like:

\begin{equation}
\mathbf{T} = 
\begin{bmatrix}\mathbf{\hat{R}} & \begin{bmatrix} d_x \\ d_y \\ d_z \end{bmatrix} \\ [ 0\quad 0 \quad 0 ] & 1\end{bmatrix}
\end{equation}
with $\mathbf{\hat{R}}$ defining the rotation and $\hat{d}$ the translation.

\section{Denavit-Hartenberg (DH) Parameters}

The Denavit–Hartenberg parameters (also called DH parameters) is a convention introduced in 1955 by Jacques Denavit and Richard Hartenberg \cite{denavit_hartenberg_1} \cite{denavit_hartenberg_2} that defines a minimal representation for attaching reference frames to the links of a spatial kinematic chain, or robot manipulator using only four parameters. Although other conventions already existed at the time they are still considered as the standard approach when dealing with spatial linkages and forward kinematics.

\begin{figure}[htb]
	\centering
	\includegraphics[scale=0.8]{Pictures/denavit_hartenberg_parameters_revolute.pdf}
	\caption{Frame convention and Denavit and Hartenberg (DH) parameters. Source: \cite{introduction_to_robotics}.}
	\label{fig:denavit_hartenberg_frame_convention}
\end{figure}

Using figure \ref{fig:denavit_hartenberg_frame_convention} and \cite{introduction_to_robotics} as reference the following ``rules" should be defined:
\begin{itemize}
\item Axis $i$ $\to$ The axis of the join connection link $i-1$ to link $i$.
\item $X_i$,$Y_i$,$Z_i$ $\to$ A coordinate system attached to the end of the link $i-1$.
\item Axis $Z_i$ $\to$ Should be chosen along the axis of the join $i$. Any direction towards the direction of the axis will do.
\item $O_i$ $\to$ Origin is located at the intersection of the common normal to $Z_{i-1}$ and $Z_i$.
\item $O'_i$ $\to$ Origin is located at the intersection of the common normal to $Z_{i}$ and $Z_{i+1}$.
\item Axis $X_i$ $\to$ Should be chosen along the common normal to axes $Z_{i-1}$ and $Z_i$. The direction should be from $Z_{i-1}$ and $Z_i$.
\item Axis $Y_i$ $\to$ Should be chosen as to complete a right handed frame.
\end{itemize}

Once the link frames have been established for every joint, the position and orientation of frame $i$ relative to frame $i-1$ are fully specified by the four Denavit and Hartenberg (DH) parameters.

This frames are also referred to as DH frames. The definitions of the four DH parameters are:

\begin{itemize}
\item $b_i$ (Joint offset) $\to$ Length of the intersections of the common normals on the joint axis $Z_i$ i.e. the measured distance between $X_i$ and $X_{i+1}$ along $Z_i$.
\item $\theta_i$ (Joint angle) $\to$ Angle between the orthogonal projections of the common normals about the $X_i$ and $X_{i-1}$. If the rotation is made counter clockwise, the angle is positive.
\item $a_i$ (Link length) $\to$ Length between $O'_i$ and $O_{i+1}$ i.e. the measured distance between the common normals to axis $Z_i$ and $Z_{i+1}$ along $X_{i+1}$.
\item $\alpha_i$ (Twist angle) $\to$ Angle about the common normal from $Z_i$ and $Z_{i+1}$ onto the plane normal to the common normal. If the rotation is made counter clockwise, the angle is positive.
\end{itemize}

\moreinfo{This description is valid for common cases of revolute joints. For a full description please consult the original articles \cite{denavit_hartenberg_1}, \cite{denavit_hartenberg_2} or \cite{introduction_to_robotics}. For more multimedia material please check the Kinematics section of the Tekkotsu framework \cite{tekkotsu_kinematics}.}

This convention defines two affine transformations that combined together allow the transformation of frame reference of link $n$ to a frame reference of link $n-1$. Entire kinematic chains can the defined this way.

Let $\mathbf{T}$ be the transformation locating the end-link, $\mathbf{Z_i}$ a transformation associated with the joint $i$ and $\mathbf{X_i}$ a transformation associated with the link $i$.

A combined transformation along a kinematic chain consisting of $n$ links form the kinematics equation:

\begin{equation}
\mathbf{T} = \mathbf{Z_1}\mathbf{X_1}\mathbf{Z_2}\mathbf{X_2}\ldots\mathbf{X_{n-1}}\mathbf{Z_n}
\end{equation}

With $\mathbf{Z_i}$ being defined as the movement of the links around a common joint axis:

\begin{equation}
\mathbf{Z_i} = \begin{bmatrix}
	\cos\alpha_i & -\sin\alpha_i & 0 & 0\\
	\sin\alpha_i & \cos\alpha_i & 0 & 0\\
	0 & 0 & 1 & b_i\\
	0 & 0 & 0 & 1
\end{bmatrix}
\end{equation}

And $\mathbf{X_i}$ being defined as the dimensions of each link in the serial chain:

\begin{equation}
\mathbf{X_i} = \begin{bmatrix}
	1 & 0 & 0 & a_{i,i+1} \\ 
	0 & \cos\theta_{i,i+1} & -\sin\theta_{i,i+1} & 0 \\ 
	0 & \sin\theta_{i,i+1} & \cos\theta_{i,i+1} & 0 \\ 
	0 & 0 & 0 & 1
\end{bmatrix}
\end{equation}

The combined DH affine transformation would result in:

\begin{equation}
\mathbf{{}^{n-1}T_n} = \left[
\begin{array}{ccc|c}
    \cos\theta_n & -\sin\theta_n \cos\alpha_n & \sin\theta_n \sin\alpha_n & a_n \cos\theta_n \\
    \sin\theta_n & \cos\theta_n \cos\alpha_n & -\cos\theta_n \sin\alpha_n & a_n \sin\theta_n \\
    0 & \sin\alpha_n & \cos\alpha_n & b_n \\
    \hline
    0 & 0 & 0 & 1
  \end{array}
\right] =
\left[
\begin{array}{ccc|c}
     &  &  &  \\
     & \mathbf{R} &  & \mathbf{T} \\
     & &  &  \\
    \hline
    0 & 0 & 0 & 1
  \end{array}
\right]
\end{equation}
where $\mathbf{R}$ and $\mathbf{T}$ correspond once again to the orientation ($3 \times 3$) and translation ($3 \times 1$).

\subsection{Base frame to End Frame $\leftrightarrow$ End frame to Base frame}
\label{sec:affine_transformation_inversion}
One important aspect of the Denavit and Hartenberg transformations and other affine transformations is that the inverse transformation is:

\begin{equation}
\mathbf{{}^{n-1}T_n}^{-1} = \mathbf{{}^{n}T_{n-1}} =
\left[
\begin{array}{ccc|c}
     &  &  &  \\
     & \mathbf{R}^\intercal &  & -\mathbf{R}^\intercal \mathbf{T} \\
     & &  &  \\
    \hline
    0 & 0 & 0 & 1
  \end{array}
\right]
\end{equation}
since $\mathbf{R}^\intercal$ is a orthogonal matrix its inverse and transpose are the same so $\mathbf{R}^\intercal = \mathbf{R}^{-1}$ and, in terms of numeric calculations, is a much cheaper operation to execute.

This nice to have property of affine transformations allows the conversion of frame links from the end effector to the base frame instead of the normal behaviour of base frame to end effector.

\section{Joints description and Forward Kinematics}
\label{sec:joints_description}
The robot has 21 DOF so it can execute various simple or complex moves like for example, walking, kicking, getting up, falling to defend a ball, rest, etc. Kinematics can be used in the planing and execution phase of these movements. With forward kinematics, one can easily find the position of the ball in the floor or the position and orientation of the head and correlate that with the horizon.

\subsection{Head joints}
\begin{figure}[H]
\centering
\mbox{
	\subfigure[Head]{
    	\label{fig:nao_specs_joints_head}
		\includegraphics[scale=0.57]{Pictures/nao_specs_joints_head.png}}
  	\quad
	\subtable[Operational range]{
		\raisebox{2.1cm}{\begin{tabular}{|l|c|}
		\hline
		\textbf{Joint Name} & \textbf{Range in $^\circ$} \\\hline
		HeadYaw 	& -119.5 $\leftrightarrow$ 119.5 \\
		HeadPitch 	& -38.5 $\leftrightarrow$ 29.5\\\hline
	\end{tabular}}}
}
\caption{NAO T21 head joints and operational range.}
\label{fig:nao_joints_head}
\end{figure}

\needspace{3\baselineskip}
\begin{lstlisting}[caption=List of ALMemory command key names for the Head joints actuators (in radian),label=lst:joint_head_actuators]
Device/SubDeviceList/HeadPitch/Position/Actuator/Value
Device/SubDeviceList/HeadYaw/Position/Actuator/Value
\end{lstlisting}

\needspace{3\baselineskip}
\begin{lstlisting}[caption=List of ALMemory value key names for the Head joints sensors (in radian),label=lst:joint_head_sensors]
Device/SubDeviceList/HeadPitch/Position/Sensor/Value
Device/SubDeviceList/HeadYaw/Position/Sensor/Value
\end{lstlisting}

\subsection{Arm joints}
\begin{figure}[H]
\centering
  	\subfigure[Right and left arm]{
    \label{fig:nao_specs_joints_arms}
	\includegraphics[scale=0.29]{Pictures/nao_specs_joints_arms.png}}
    \quad
	\subtable[Operational range]{
	\raisebox{3.5cm}{\begin{tabular}{|l|c|}
		\hline
		\textbf{Joint Name} & \textbf{Range in $^\circ$} \\\hline
		LShoulderPitch & -119.5 $\leftrightarrow$ 119.5\\
		LShoulderRoll & -18 $\leftrightarrow$ 76\\
		LElbowYaw & -119.5 $\leftrightarrow$ 119.5\\
		LElbowRoll & -88.5 $\leftrightarrow$ -2\\
		RShoulderPitch & -119.5 $\leftrightarrow$ 119.5\\
		RShoulderRoll & -76 $\leftrightarrow$ 18\\
		RElbowYaw & -119.5 $\leftrightarrow$ 119.5\\
		RElbowRoll & -2 $\leftrightarrow$ 88.5\\
		L(R)WristYaw & disabled\\\hline
	\end{tabular}}}
\caption{NAO T21 arm joints and operational range.}
\label{fig:nao_joints_arms}
\end{figure}

%\needspace{3\baselineskip}
\begin{lstlisting}[caption=List of ALMemory command key names for the Left and Right arms joints actuators (in radian),label=lst:joint_left_right_arms_actuators]
Device/SubDeviceList/LShoulderPitch/Position/Actuator/Value
Device/SubDeviceList/LShoulderRoll/Position/Actuator/Value
Device/SubDeviceList/LElbowYaw/Position/Actuator/Value
Device/SubDeviceList/RShoulderPitch/Position/Actuator/Value
Device/SubDeviceList/RShoulderRoll/Position/Actuator/Value
Device/SubDeviceList/RElbowYaw/Position/Actuator/Value
\end{lstlisting}

%\needspace{3\baselineskip}
\begin{lstlisting}[caption=List of ALMemory value key names for the Left and Right arms joints sensors (in radian),label=lst:joint_left_right_arms_sensors]
Device/SubDeviceList/LShoulderPitch/Position/Sensor/Value
Device/SubDeviceList/LShoulderRoll/Position/Sensor/Value
Device/SubDeviceList/LElbowYaw/Position/Sensor/Value
Device/SubDeviceList/RShoulderPitch/Position/Sensor/Value
Device/SubDeviceList/RShoulderRoll/Position/Sensor/Value
Device/SubDeviceList/RElbowYaw/Position/Sensor/Value
\end{lstlisting}

\subsection{Pelvis and legs joints}

\begin{figure}[H]
\centering
  	\subfigure[Pelvis, left and right leg]{
    \label{fig:nao_specs_joints_pelvis_and_legs}
	\includegraphics[scale=0.57]{Pictures/nao_specs_joints_pelvis_and_legs.png}}
    \quad
	\subtable[Operational range]{
	\raisebox{8cm}{\begin{tabular}{|l|c|}
		\hline
		\textbf{Joint Name} & \textbf{Range in $^\circ$} \\\hline
		LHipYawPitch & -65.62 $\leftrightarrow$ 42.44\\
		RHipYawPitch & -65.62 $\leftrightarrow$ 42.44\\
		LHipRoll & -21.74 $\leftrightarrow$ 45.29\\
		LHipPitch & -101.63 $\leftrightarrow$ 27.73\\
		LKneePitch & -5.29 $\leftrightarrow$ 121.04\\
		LAnklePitch & -68.15 $\leftrightarrow$ 52.86\\
		LAnkleRoll & -22.79 $\leftrightarrow$ 44.06\\
		RHipRoll & -42.30 $\leftrightarrow$ 23.76\\
		RHipPitch & -101.54 $\leftrightarrow$ 27.82\\
		RKneePitch & -5.90 $\leftrightarrow$ 121.47\\
		RAnklePitch & -67.97 $\leftrightarrow$ 53.40\\
		RAnkleRoll & -45.03 $\leftrightarrow$ 22.27\\\hline
	\end{tabular}}}
\caption{NAO T21 pelvis, left and right leg joints and operational range.}
\label{fig:nao_joints_pelvis_and_legs}
\end{figure}

%\needspace{3\baselineskip}
\begin{lstlisting}[caption=List of ALMemory command key names for the Left and Right legs joints actuators (in radian),label=lst:joint_left_right_legs_actuators]
Device/SubDeviceList/LHipRoll/Position/Actuator/Value
Device/SubDeviceList/LHipPitch/Position/Actuator/Value
Device/SubDeviceList/LKneePitch/Position/Actuator/Value
Device/SubDeviceList/LAnklePitch/Position/Actuator/Value
Device/SubDeviceList/LAnkleRoll/Position/Actuator/Value
Device/SubDeviceList/RHipRoll/Position/Actuator/Value
Device/SubDeviceList/RHipPitch/Position/Actuator/Value
Device/SubDeviceList/RKneePitch/Position/Actuator/Value
Device/SubDeviceList/RAnklePitch/Position/Actuator/Value
Device/SubDeviceList/RAnkleRoll/Position/Actuator/Value
\end{lstlisting}

%\needspace{3\baselineskip}
\begin{lstlisting}[caption=List of ALMemory value key names for the Left and Right legs joints sensors (in radian),label=lst:joint_left_right_legs_sensors]
Device/SubDeviceList/LHipRoll/Position/Sensor/Value
Device/SubDeviceList/LHipPitch/Position/Sensor/Value
Device/SubDeviceList/LKneePitch/Position/Sensor/Value
Device/SubDeviceList/LAnklePitch/Position/Sensor/Value
Device/SubDeviceList/LAnkleRoll/Position/Sensor/Value
Device/SubDeviceList/RHipRoll/Position/Sensor/Value
Device/SubDeviceList/RHipPitch/Position/Sensor/Value
Device/SubDeviceList/RKneePitch/Position/Sensor/Value
Device/SubDeviceList/RAnklePitch/Position/Sensor/Value
Device/SubDeviceList/RAnkleRoll/Position/Sensor/Value
\end{lstlisting}

\subsection{Masses of link/joints (body parts)}
\begin{center}
\begin{longtable}{|l|S[table-format=1.5,group-digits=false]|S|S|S|}
\caption{NAO T21 Masses of link/joints along with its corresponding Center of Mass.}\label{tab:nao_body_masses_com}\\\hline
\multicolumn{5}{|c|}{\textbf{NAO T21 Masses of link/joints}} \\\hline
\multicolumn{1}{|l|}{\textbf{Frame Name}} & \multicolumn{1}{c|}{\textbf{Mass (Kg)}} & \multicolumn{1}{c|}{\textbf{CoMx (mm)}} & \multicolumn{1}{c|}{\textbf{CoMy (mm)}} & \multicolumn{1}{c|}{\textbf{CoMz (mm)}}\\\hline
\endfirsthead

\multicolumn{5}{c}{{\tablename\ \thetable{} -- continued from previous page}} \\\hline
\multicolumn{1}{|l|}{\textbf{Frame Name}} & \multicolumn{1}{c|}{\textbf{Mass (Kg)}} & \multicolumn{1}{c|}{\textbf{CoMx (mm)}} & \multicolumn{1}{c|}{\textbf{CoMy (mm)}} & \multicolumn{1}{c|}{\textbf{CoMz (mm)}}\\\hline
\endhead

\hline \multicolumn{5}{|r|}{Continued on next page} \\\hline
\endfoot
\endlastfoot

Torso & 1.03948 & -4.15 & 0.07 & 42.58\\
HeadYaw & 0.05930 & -0.02 & 0.17 & 25.56\\
HeadPitch & 0.52065 & 1.2 & -0.84 & 53.53\\
RShoulderPitch & 0.06996 & -1.78 & 24.96 & 0.18\\
RShoulderRoll & 0.12309 & 18.85 & -5.77 & 0.65\\
RElbowYaw & 0.05971 & -25.6 & 0.01 & -0.19\\
RElbowRoll & 0.185 & 65.36 & -0.34 & -0.02\\
LShoulderPitch & 0.06996 & -1.78 & -24.96 & 0.18\\
LShoulderRoll & 0.12309 & 18.85 & 5.77 & 0.65\\
LElbowYaw & 0.05971 & -25.6 & -0.01 & -0.19\\
LElbowRoll & 0.185 & 65.36 & 0.34 & -0.02\\
RHipYawPitch & 0.07117 & -7.66 & 12 & 27.17\\
RHipRoll & 0.1353 & -16.49 & -0.29 & -4.75\\
RHipPitch & 0.39421 & 1.32 & -2.35 & -53.52\\
RKneePitch & 0.29159 & 4.22 & -2.52 & -48.68\\
RAnklePitch & 0.13892 & 1.42 & -0.28 & 6.38\\
RAnkleRoll & 0.16175 & 25.4 & -3.32 & -32.41\\
LHipYawPitch & 0.07117 & -7.66 & -12 & 27.17\\
LHipRoll & 0.1353 & -16.49 & 0.29 & -4.75\\
LHipPitch & 0.39421 & 1.32 & 2.35 & -53.52\\
LKneePitch & 0.29159 & 4.22 & 2.52 & -48.68\\
LAnklePitch & .13892 & 1.42 & 0.28 & 6.38\\
LAnkleRoll & 0.16175 & 25.4 & 3.32 & -32.41\\\hline
Total Mass & 4.88083 & \multicolumn{3}{c|}{} \\\hline
\end{longtable}
\end{center}

%\subsection{DH NAO Parameters}
%\todo[inline]{Insert a table with the DH parameters that were calculated.}

\subsection{Implementation}
In the \classstyle{AgentModel} class, the base class of the Forward Kinematics is a interface named \classstyle{iBodyPart}. This class contains a transformation matrix that forms the actual Forward Kinematics ${}^{n-1}\mathbf{T}_n$ conversion and a calculated three-dimensional vector with the relative position from the Head referential to this \classstyle{iBodyPart} that simply is the translation ($\mathbf{Z}$) part from the transformation.

The \classstyle{AgentModel} uses a slightly different convention for the Forward Kinematics than the Denavit-Hartenberg parameters. First the \classstyle{Head} body part is processed. This serves as the base reference for all the other \classstyle{iBodyPart} so the transformation in the \classstyle{Left Foot} will transform a point in the \classstyle{Left Foot} referential to the \classstyle{Head} referential.

The two classes (\classstyle{BodyPart} and the \classstyle{Joint}) that implement this interface have different ways of calculating the transformation matrix. 

The \classstyle{BodyPart} class uses the joint anchor to do the initial translation and then uses the parent's transformation to convert the referentials. Each joint will be processed after this so all the joints in the same level from the same parent are processed one at the time and only after that the algorithm processes the next level.

\bigskip

\begin{algorithm}[H]
\Begin{
initialization\;
\For{joint $\in$ bodyPart.joints}{
	joint.transform $\longleftarrow$ joint.transform $\times$ Z(joint.anchor1)\;
	joint.relPosition $\longleftarrow$ joint.transform.toVector3D\;
}
\caption{Body Part transformation creation.\label{code:bodypart}}}
\end{algorithm}

The \classstyle{Joint} on the other hand will translate the body part attached to with based on the second anchor and them rotate the same body part with the joint angle and axis:

\bigskip

\begin{algorithm}[H]
\Begin{
initialization\;
joint.body.transform $\longleftarrow$ joint.transform $\times$ R(joint.axis,joint.angle) $\times$ Z(joint.anchor2)\;
joint.body.relPosition $\longleftarrow$ joint.transform.toVector3D\;
\caption{Joint Part transformation creation.\label{code:bodypart}}}
\end{algorithm}

The file \classstyle{nao.xml} was updated (\ref{ut:nao_xml}) from the v3.2 simulation version to the real device v3.3. Besides some small changes in the dimensions and max/min angles, the head joint anchor point was changed. In the simulation the head joint is centred in the head and in the real device the head is connected to the neck \classstyle{BodyPart}.