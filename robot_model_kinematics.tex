\section{Transformations}
In order to be able to develop higher level functions like the DistanceMap converter some basic transformations were developed. They are described in detail in the following sections.

\subsection{Torso to Vertical}
Calculate the affine transformation from the Torso referential to the Vertical referential. The vertical referential uses the rotation angles from the Inertial Unit Filtered Data. Previously described in \ref{sec:unfiltered_filtered_data}.

\begin{equation}
\mathbf{{}^{\small\text{Torso}}T_{\small\text{Vertical}}} = \mathbf{R_y}(\text{InertialFilteredValues}_y)\times\mathbf{R_x}(\text{InertialFilteredValues}_x)
\end{equation}

\begin{lstlisting}[caption=Torso to Vertical transformation from the AgentModel class,language=c++,label=transformation_torso_to_vertical]
Matrix4D AgentModel::getTransformationFromTorsoToVertical()
{
	Vector inertialFilteredValues = getInertialUnitFilteredValues();
	return Matrix4D::rotationY(Rad2Deg(inertialFilteredValues.y))
			* Matrix4D::rotationX(Rad2Deg(inertialFilteredValues.x));
}
\end{lstlisting}

\subsection{Head to Torso}
Calculate the affine transformation from the Head referential to the Torso referential. Based on the properties already discussed in \ref{sec:affine_transformation_inversion}.
\begin{equation}
\mathbf{{}^{\small\text{Head}}T_{\small\text{Torso}}} = \mathbf{{}^{\small\text{Torso}}T_{\small\text{Head}}}^{-1}
\end{equation}

\begin{lstlisting}[caption=Head to Torso transformation from the AgentModel class,language=c++,label=transformation_head_to_torso]
Matrix4D AgentModel::getTransformationFromHeadToTorso()
{
	// Assumes the UpdatePosture cycle already runned.
	BodyPart* torsoPart = m_parts[Types::ilTorso];

	return torsoPart->transform.inverse();
}
\end{lstlisting}

\subsection{Head to Vertical}
Calculate the affine transformation from the Head referential to the Vertical referential.

\begin{equation}
\mathbf{{}^{\small\text{Head}}T_{\small\text{Vertical}}} = \mathbf{{}^{\small \text{Torso}}T_{\small\text{Vertical}}} \times \mathbf{{}^{\small\text{Head}}T_{\small\text{Torso}}}
\end{equation}

\begin{lstlisting}[caption=Head to Vertical transformation from the AgentModel class,language=c++,label=transformation_head_to_vertical]
Matrix4D AgentModel::getHeadReferenceBasedOnGravity()
{
	Matrix4D torsoToVertical = getTransformationFromTorsoToVertical();
	Matrix4D headToTorso = getTransformationFromHeadToTorso();
	
	Matrix4D result = torsoToVertical * headToTorso;
	
    // Clear the translation	
	result.set(0,3,0);
	result.set(1,3,0);
	result.set(2,3,0);

	return result;
}
\end{lstlisting}

\subsection{Camera to Vertical}
Calculate the affine transformation from the Camera (Top or Bottom) referential to the Vertical referential, assuming that:
\begin{itemize}
\item $cam_{PitchAngle}$ $\to$ The calibrated camera pitch angle.
\item $cam_{YawAngle}$ $\to$ The calibrated camera yaw angle.
%\item $cam_X$ $\to$ The camera offset in the $x$ axis defined in figure \ref{fig:nao_camera_position_3_x} and \ref{fig:nao_camera_position_4}.
%\item $cam_Y$ $\to$ The camera offset in the $y$ axis defined in figure \ref{fig:nao_camera_position_3_x} and \ref{fig:nao_camera_position_4}.
%\item $cam_Z$ $\to$ The camera offset in the $z$ axis defined in figure \ref{fig:nao_camera_position_3_x} and \ref{fig:nao_camera_position_4}.
\item Camera offsets described in figure \ref{fig:nao_camera_position_3_x} and \ref{fig:nao_camera_position_4} are not taken into consideration at this time.
\end{itemize}
\begin{equation}
\mathbf{{}^{\small\text{Camera}}T_{\small\text{Vertical}}} = \mathbf{{}^{\small \text{Head}}T_{\small\text{Vertical}}} \times \mathbf{R_y}(\text{cam}_{\text{PitchAngle}})\times\mathbf{R_z}(\text{cam}_{\text{YawAngle}})
\end{equation}

\begin{lstlisting}[caption=Camera to Vertical transformation from the AgentModel class,language=c++,label=transformation_camera_to_vertical]
Matrix4D AgentModel::getCameraReferenceBasedOnGravity(int cam)
{
	float camPitchAngle, camYawAngle, camX, camY, camZ;
	
	if (cam == 1)
	{
		camPitchAngle = myTopPitch;
		camYawAngle = myTopYaw;
	}
	else
	{
		camPitchAngle = myBottomPitch;
		camYawAngle = myBottomYaw;
	}
	
	return getHeadReferenceBasedOnGravity()
		*Matrix4D::rotationY(camPitchAngle)
		*Matrix4D::rotationZ(camYawAngle);
}
\end{lstlisting}

\subsection{Estimated Height}
Calculate the estimated height of the robot head to the ground taking into consideration the current joints values and orientation with the vertical, assuming that: 
\begin{itemize}
\item At least one of the foots are in the contact with the ground.
\item The biggest distance corresponds to the foot that is further away.
\end{itemize}

\begin{subequations}
\begin{align}
\vec{\text{head}}_{\text{vertical}} &= \mathbf{{}^{\small \text{Head}}T_{\small \text{Vertical}}} \times \vec{\text{head}}_{\text{relativePosition}}\\
%
\vec{\text{leftfoot}}_{\text{vertical}} &= \mathbf{{}^{\small \text{Head}}T_{\small \text{Vertical}}} \times \vec{\text{head}}_{\text{relativePositionToHead}}\\
%
\vec{\text{rightfoot}}_{\text{vertical}} &= \mathbf{{}^{\small \text{Head}}T_{\small \text{Vertical}}} \times \vec{\text{head}}_{\text{relativePositionToHead}}\\
%
\text{headToFoot}_{\text{left}} &= {\text{head}_{\text{vertical}}}_z - {\text{leftfoot}_{\text{vertical}}}_z\\
%
\text{headToFoot}_{\text{right}} &= {\text{head}_{\text{vertical}}}_z - {\text{rightfoot}_{\text{vertical}}}_z\\
%
\text{height} &= max(\text{headToFoot}_{\text{left}}, \text{headToFoot}_{\text{right}})
\end{align}
\end{subequations}

\begin{lstlisting}[caption=Estimated Height from the head to the ground function from the AgentModel class,language=c++,label=function_estimated_height]
// Calculate the relative height of the camera from the ground
//	1)	calculate for each foot and use the foot with the biggest distance
float AgentModel::getEstimatedHeightFromGround()
{
	BodyPart* head  = getBodyPart(Types::ilHead);
	BodyPart* footL = getBodyPart(Types::ilLFoot);
	BodyPart* footR = getBodyPart(Types::ilRFoot);

	Matrix4D headTransform = getHeadReferenceBasedOnGravity();

	Vector3f headInVerticalReference = headTransform * head->relPosition;
	Vector3f footLInVerticalReference = headTransform * footL->relPosition;
	Vector3f footRInVerticalReference = headTransform * footR->relPosition;

	float headTofootL = headInVerticalReference.getZ() 
						- footLInVerticalReference.getZ();
	float headTofootR = headInVerticalReference.getZ() 
						- footRInVerticalReference.getZ();

	// TODO: Check all Joints/BodyParts and choose the lowest relPosdistance_map_converter_1ition;
	// TODO: Check the foot that has contact and possibly check the foot
	// extremities for the lowest point in the foot
	return max(headTofootL, headTofootR);
}
\end{lstlisting}

\subsection{DistanceMap converter}
\label{sec:distancemap_converter}
This function was divided into three small steps. First calculate the relative pixel position in the \classstyle{CCD} (\classstyle{getPixelRelativeToMiddleOfCCD}), next use the \classstyle{CCD} orientation with the vertical transformation and finally use known trigonometry properties to find the point (assuming it lies in the floor). The distance map converter was initially based on the article \cite{distance_map_converter} with further improvements regarding the orientation of the camera with the vertical. Since the camera is attached to the head and the head to the torso, the camera yaw, pitch and roll with the vertical might be different in each call.

\begin{figure}[H]
\centering
  \subfigure[]{
    \label{fig:distance_map_converter_3}
	\includegraphics[scale=1]{Pictures/distance_map_converter_3.pdf}}
  \qquad
  \subfigure[]{
    \label{fig:distance_map_converter_4}
	\includegraphics[scale=1]{Pictures/distance_map_converter_4.pdf}}
\caption{(a) Schematic view of a detail over the lens, CCD and focus point from the perspective system being (a) the left side view and (b) the right top-down view. Source (a)-(b): \cite{distance_map_converter}.}
\label{fig:pixel_position_relative_to_middle_ccd}
\end{figure}

\subsubsection{Pixel position relative to the middle of the CCD}
This function check the current resolution and if the resolution is even or odd. Returns the pixel value relative to the center of the CCD. A schematic representation is shown in figure~\ref{fig:pixel_position_relative_to_middle_ccd}.

\begin{lstlisting}[caption=Pixel position relative to the middle of the CCD from the DistanceMapConverter class,language=c++,label=pixel_relative_to_middle_ccd]
Vector DistanceMapConverter::getPixelRelativeToMiddleOfCCD(Vector pixel)
{
	Vector resolution = cameraDriver.getActiveResolution();
	int halfWidth = resolution.x / 2;
	int halfHeight = resolution.y / 2;
	int resWidth = (int) resolution.x;
	int resHeight = (int) resolution.y;
	int isEvenWidth = (resWidth % 2);
	int isEvenHeight = (resHeight % 2);

	Vector ret = Vector(
			halfWidth + (isEvenWidth ? 0 : 1) - (int) pixel.x,
			halfHeight + (isEvenHeight ? 0 : 1) - (int) pixel.y
			);

	if (!isEvenWidth)
	{
		if ((int) pixel.x < halfWidth) ret.x--; // Pixel starts at 0
		else ret.x -= 2; // Compensate the odd when in the lower

		if ((int) pixel.y < halfHeight) ret.y--; // Pixel starts at 0
		else ret.y -= 2; // Compensate the odd when in the lower
	}

	return Vector(ret.x, -ret.y);
}
\end{lstlisting}

\subsubsection{Camera orientation with the Vertical}
\label{sec:camera_location}
Calculate the camera angles with the vertical ($pitch$, $roll$, $yaw$) based on the current body orientation. Assuming that $\mathbf{camera\,}_{inVertical}$ is a $3 \times 3$ matrix with the orientation ($\hat{\mathbf{R}}$) of the camera with the vertical.

The resulting method name would be \classstyle{getCameraOrientationInVertical(int camera)}.

Using the scalar product to find the angle between the body orientation and each of the $x$-axis vector $(1,0,0)$ and the $z$-axis vector $(0,0,1)$ is a relatively simple task. The only concern is to check if the angle needs to be inverted.

\begin{subequations}
\begin{flalign}
&\text{\qquad\qquad\qquad\quad} & \vec{a} \cdot \vec{b} &= \|\vec{a}\| \|\vec{b}\| \cos_{\angle\vec{a b}}\\[1em]
&& \angle_{\vec{a b}} &= \arccos\left(\frac{\vec{a} \cdot \vec{b}}{\|\vec{a}\| \|\vec{b}\|}\right)\\[0.5em]
& \lefttext{\textsl{vectors are unitary}} & \angle_{\vec{a b}} &= \arccos\left(\hat{a} \cdot \hat{b} \right)\\[1em]
&& \hat{x} &= (1,0,0)\quad\quad \hat{y} = (0,1,0)\quad & \hat{z} =& (0,0,1) \\[1em]
&& \hat{x}_{\text{camera}} &= \mathbf{camera\,}_{inVertical} \times \hat{x}\\
&& \hat{z}_{\text{camera}} &= \mathbf{camera\,}_{inVertical} \times \hat{z}\\[1em]
&& \hat{y}_{\text{vert}} &= \hat{z} \times \hat{x}_{\text{camera}} \\
&& \hat{z}_{\text{vert}} &= \hat{x} \times \hat{y}_{\text{vert}} \\
\displaybreak[1] && \hat{x}_{\text{vertRot}} &= \hat{y}_{\text{vert}} \times \hat{z} \\[1em]
& \lefttext{\textsl{for the pitch}} & \text{ccd}_{\angle x} &= \pm \arccos(\hat{z}_{\text{vert}} \cdot \hat{z} ) , &-&\textsl{ if } (\hat{x}_{\text{vertRot}} \cdot \hat{z}_{\text{vert}}) < 0 & \\
& \lefttext{\textsl{for the roll}} & \text{ccd}_{\angle y} &= \pm \arccos(\hat{z}_{\text{vert}} \cdot \hat{z}_{\text{camera}} ) , &-&\textsl{ if } (\hat{z}_{\text{camera}} \cdot \hat{y}_{\text{vert}}) < 0 & \\
& \lefttext{\textsl{for the yaw}} & \text{ccd}_{\angle z} &= \pm \arccos(\hat{x}_{\text{vertRot}} \cdot \hat{x} ) , &-&\textsl{ if } (\hat{x}_{\text{camera}} \cdot \hat{y}) < 0 & \\[1em]
& \lefttext{\textsl{and finally}} & \text{ccd}_{\angle} &= \left( \text{ccd}_{\angle x}, \text{ccd}_{\angle y}, \text{ccd}_{\angle z} \right)
\end{flalign}
\end{subequations}

\begin{figure}[H]
\centering
  \subfigure[]{
    \label{fig:distance_map_converter_1}
	\includegraphics[scale=0.8]{Pictures/distance_map_converter_1.pdf}}
  \subfigure[]{
    \label{fig:distance_map_converter_2}
	\includegraphics[scale=0.8]{Pictures/distance_map_converter_2.pdf}}
\caption{(a) robot and its perspective system schematic side view, (b) schematic top-down view of the robot and perspective system. Source (a)-(b): \cite{distance_map_converter}.}
\end{figure}

\subsubsection{Relative position on the ground of a camera pixel} 
Calculate the point $(x,y,\text{ground})$ relative to the robot body having into consideration the robot position, orientation, camera index (top or bottom) and camera calibration parameters.

This is the final step for the \classstyle{DistanceMap} converter class. Figure \ref{fig:distance_map_converter_1} and \ref{fig:distance_map_converter_2} represents a generic conversion when the camera angle and pitch are known. As previously described, \cite{distance_map_converter} only considers that the camera is fixed to the robot chassis and that the robot only slides (rolls) in the ground thus always having it's $h$-axis pointing up and being perpendicular with the ground. 

In a humanoid robot this assumption is far from the reality. A walking robot will introduce changes in the torso orientation to maintain balance or the robot might be looking down preparing a kick.

Keeping the definition of the article:
\begin{itemize*}
\item $h_{\text{offset}}$ $\to$ distance from the camera to the ground;
\item $r_{\text{offset}}$ $\to$ radial distance from the camera to the robot center;
\item $\alpha_{\text{offset}}$ $\to$ angular offset of the camera along $\alpha$ axis;
\item $y_{\text{offset}}$ $\to$ distance from the center of the robot to the point in the center of the image projected on the ground;
\item $\text{ang}_{\alpha n}$ $\to$ angle measured from the $\alpha_{\text{offset}}$ along $\alpha$ axis, relative to $\text{pixel}_{\text{n}}$;
\item $\text{ang}_{\theta m}$ $\to$ angle measured from the robot front along $\theta$ axis, relative to $\text{pixel}_{\text{n}}$;
\item $\text{distance}_{yn}$ $\to$ distance from the center of the robot to the $\text{pixel}_{\text{n}}$, projected on the ground;
\item $\text{distance}_{xm}$ $\to$ distance from the center of the robot to the $\text{pixel}_{\text{m}}$, projected on the ground;
\item $\text{focal}_{\text{len}}$ $\to$ distance between lens and CCD;
\item $\text{pixel}_{\text{n}}$ $\to$ number of pixels ($n$) along a CCD column;
\item $\text{pixel}_{\text{m}}$ $\to$ number of pixels ($m$) along a CCD row;
\end{itemize*}

A detailed description of the algorithm follows next and the \classstyle{C++} implementation is attached in listing . 

First the method gets the $\vec{camera}_{\text{inVertical}}$ vector that describes the angles the camera is doing with the vertical.

Then it calculates the offsets of the camera position and orientation based on the camera type namely:
\begin{align}
\alpha_{\text{offset}} &= 90^\circ - {\text{camera}_{\text{inVertical}}}_y (\text{roll})\\
h_{\text{offset}} &= \text{estimated height of the head to the ground}\\
r_{\text{offset}} &= \text{distance from the head joint to the camera}
\end{align}

If the pixel to detect is of a ball (\classstyle{ball} parameter), decrease the $r_{\text{offset}}$ with the ball diameter to give a better approach since the pixel if not in the ground.

Next rotate the 2D $\vec{\text{pixel}}_{relative}$ vector based on the ${\text{camera}_{\text{inVertical}}}_y$ (pitch) to compensate the inclination of the body.

After that $x_{\text{relativeToCamera}}$, $x_{\text{diagonal}}$, $y_{\text{relativeToCamera}}$ and $z_{\text{relativeToCamera}}$ are calculated as: 
\begin{align}
\displaystyle x_{\text{relativeToCamera}} &= r_{\text{offset}} + h_{\text{offset}} \times \tan\left(\alpha_{\text{offset}} - \arctan\left(\text{pixel}_{\text{n}} \times \text{pixel}_{\text{height}}, \text{focal}_{\text{len}}\right)\right)\\
x_{\text{diagonal}} &= \sqrt{
\left(x_{\text{relativeToCamera}} - r_{\text{offset}}\right)^2 + h_{\text{offset}}^2}\\
y_{\text{relativeToCamera}} &= \text{pixel}_{\text{m}} \times x_{\text{diagonal}} \times \text{pixel}_{\text{width}} \times \nonumber\\ 
& \qquad\qquad\times \cos\left(\alpha_{\text{offset}} - \dfrac{\arctan\left( \dfrac{x_{\text{relativeToCamera}}-r_{\text{offset}}}{h_{\text{offset}}}\right)}{\text{focal}_{\text{len}}}\right)\\
z_{\text{relativeToCamera}} &= 0
\end{align}

The final step is to rotate the point in the floor based on the ${\text{camera}_{\text{inVertical}}}_y$ (yaw) as:
\begin{align}
\displaystyle \text{relative}_{\text{robotFront}} &= \mathbf{R}_z({\text{camera}_{\text{inVertical}}}_z) \times (x_{\text{relativeToCamera}},y_{\text{relativeToCamera}},z_{\text{relativeToCamera}})
\end{align}

\begin{lstlisting}[caption=relative position on the ground from a camera pixel from the DistanceMapConverter class,language=c++,label=relative_position_on_the_ground]
Vector DistanceMapConverter::getDistance(Vector pixel, bool ball)
{
	float alphaOffset = Deg2Rad(90.0f);
	float hOffset;
	float rOffset = 0.0f;
	Vector3f ccdOri = agentModel.getCameraOrientationInVertical(cameraType);
	
	if (cameraType == LOWER_CAMERA)
	{
		hOffset		= agentModel.bottomCameraPoseBuffer.at(bufferIndex).headHeight;
		hOffset		+= DISTANCE_HEAD_JOINT_TO_BOTTOM_CAMERA_Z 
					   - DISTANCE_HEAD_JOINT_TO_TOP_CAMERA_Z;
		// FIXME: This rOffset considers that the x=0 is the plane YoZ that passes 
		//        trough the camera, not much correct when head is turned
		rOffset		+= DISTANCE_HEAD_JOINT_TO_BOTTOM_CAMERA_X;
	}
	else
	{
		hOffset		= agentModel.topCameraPoseBuffer.at(bufferIndex).headHeight;
		// FIXME: This rOffset considers that the x=0 is the plane YoZ that passes 
		//        trough the camera, not much correct when head is turned
		rOffset		+= DISTANCE_HEAD_JOINT_TO_TOP_CAMERA_X;
	}
	
	alphaOffset -= Deg2Rad(ccdOri.y);
	
	if (ball)
		hOffset		-= 2 x BALL_RADIUS;

	float focalLength = cameraDriver.getFocalLength(); // in mts
	Vector resolution = cameraDriver.getActiveResolution();

	Vector pixelRelative = getPixelRelativeToMiddleOfCCD(pixel);
	
	//Apply the CCD roll as pixel rotation
	if (cameraType == LOWER_CAMERA)
		pixelRelative = pixelRelative.rotate(-ccdOri.x 
		                +ANGLE_HEAD_JOINT_TO_BOTTOM_CAMERA_X);
	else
		pixelRelative = pixelRelative.rotate(-ccdOri.x
		                +ANGLE_HEAD_JOINT_TO_TOP_CAMERA_X);
	
	float pixeln = pixelRelative.y;
	float pixelm = pixelRelative.x;

	float pixelWidth = cameraDriver.getPixelSize().x;  // in mts
	float pixelHeight = cameraDriver.getPixelSize().y; // in mts
	
	Vector3f relativeToCamera;
	relativeToCamera.x = rOffset + hOffset 
		* tan(alphaOffset - atan2(pixeln * pixelHeight, focalLength));

	double xDiag = sqrt(
	    (relativeToCamera.x-rOffset) * (relativeToCamera.x-rOffset) 
	    + hOffset*hOffset
	);
	    
	relativeToCamera.y = pixelm * xDiag * pixelWidth 
		* cos(alphaOffset-atan( (relativeToCamera.x-rOffset) /hOffset))
	    / focalLength;
	relativeToCamera.z = 0;

	Vector3f relativeToRobotFront = Matrix4D::rotationZ(ccdOri.z) 
	                                * relativeToCamera;

	return relativeToRobotFront.to2d();
}
\end{lstlisting}
